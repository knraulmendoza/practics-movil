import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/Select.dart';
import 'package:practics_movil/src/helpers/Connection.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:practics_movil/src/services/PersonService.dart';
import 'package:reactive_forms/reactive_forms.dart';

class PerfilController extends GetxController {
  final global = Get.find<GlobalsController>();
  final form = FormGroup({
    'gender': FormControl<String>(
      validators: [Validators.required],
    ),
    'phone': FormControl<String>(
      validators: [Validators.required, Validators.maxLength(10)],
    ),
    'address': FormControl<String>(
      validators: [Validators.required],
    ),
  });
  final genders = [
    DropdownMenuItem(child: Text('Masculino'), value: '1'),
    DropdownMenuItem(child: Text('Femenina'), value: '2'),
  ];
  @override
  void onInit() {
    super.onInit();
    this.form.control('gender').value =
        this.global.person.value.gender.toString();
    this.form.control('phone').value = this.global.person.value.phone;
    this.form.control('address').value = this.global.person.value.address;
  }

  updatePerson() async {
    this.global.person.value.gender = int.parse(this.form.value['gender']);
    this.global.person.value.phone = this.form.value['phone'];
    this.global.person.value.address = this.form.value['address'];

    if (this.global.connection.value) {
      if (await Connection.connection) {
        LoadingDialog.showLoadingDialog(
            text: 'Actualizando los datos personales ...');
        final resp = await personService.updatePerson(this.global.person.value);
        Get.back();
        if (resp.data.state) {
          ResponseWidget.snackbar(
              title: 'Actualización',
              msg: 'Se actualizarón los datos exitosamente',
              type: TypeDialog.OK);
        } else {
          ResponseWidget.snackbar(
              title: 'Actualización',
              msg:
                  'Error en el servidor no se pudo actualizar intente mas tarde',
              type: TypeDialog.ERROR);
        }
      } else {
        ResponseWidget.snackbar(
            title: 'Conexión a internet',
            msg: 'Estas sin conexión a internet',
            type: TypeDialog.ERROR);
      }
    } else {
      ResponseWidget.snackbar(
          title: 'Conexión a internet',
          msg: 'Debes habilitar la conexión internet desde el menú',
          type: TypeDialog.ERROR);
    }
  }
}
