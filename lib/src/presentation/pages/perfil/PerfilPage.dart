import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/pages/perfil/PerfilController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:reactive_forms/reactive_forms.dart';

class PerfilPage extends GetView<PerfilController> {
  @override
  Widget build(BuildContext context) {
    Get.put(PerfilController());
    final global = Get.find<GlobalsController>();
    final currentUser = jsonDecode(GetStorage().read('currentUser'));
    return Scaffold(
      appBar: AppBarWidget(
          title: 'Perfil',
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.chevron_left, color: Colors.blue,),
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () => {Get.toNamed('/')},
              );
            },
          )),
      body: ListView(
        padding: EdgeInsets.all(10),
        children: [
          Text('Nombre',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
          Text(global.person.value.fullName, style: TextStyle(fontSize: 18)),
          SizedBox(
            height: 10,
          ),
          Text('Tipo Documento',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
          Text(global.person.value.documentTypeText,
              style: TextStyle(fontSize: 18)),
          SizedBox(
            height: 10,
          ),
          Text('Documento',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
          Text(global.person.value.document, style: TextStyle(fontSize: 18)),
          SizedBox(
            height: 10,
          ),
          Text('Carrera',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
          Text(currentUser['careerName'], style: TextStyle(fontSize: 18)),
          Theme(
            data: ThemeData(primaryColor: Colors.green),
            child: ReactiveForm(
              formGroup: controller.form,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text('Genero',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    child: ReactiveDropdownField<String>(
                        formControlName: 'gender',
                        hint: Text(
                          'Genero',
                          style:
                              TextStyle(color: Color.fromRGBO(0, 147, 57, 1)),
                        ),
                        isExpanded: true,
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(8.0),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          hintStyle:
                              TextStyle(color: Color.fromRGBO(0, 147, 57, 1)),
                        ),
                        items: this.controller.genders),
                  ),
                  Text('Telefono',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  SizedBox(
                    height: 5,
                  ),
                  ReactiveTextField(
                    formControlName: 'phone',
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(8.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      hintText: 'Celular',
                      hintStyle:
                          TextStyle(color: Color.fromRGBO(0, 147, 57, 1)),
                      prefixIcon: Icon(Icons.phone),
                    ),
                    keyboardType: TextInputType.phone,
                    validationMessages: (control) => {
                      ValidationMessage.required: 'Este campo es obligatorio',
                    },
                    textInputAction: TextInputAction.next,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text('Dirección',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  SizedBox(
                    height: 5,
                  ),
                  Theme(
                    data: ThemeData(primaryColor: Colors.green),
                    child: ReactiveTextField(
                      formControlName: 'address',
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(8.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        hintText: 'Dirección',
                        hintStyle:
                            TextStyle(color: Color.fromRGBO(0, 147, 57, 1)),
                        prefixIcon: Icon(Icons.phone),
                      ),
                      validationMessages: (control) => {
                        ValidationMessage.required: 'Este campo es obligatorio',
                      },
                      textInputAction: TextInputAction.next,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  ReactiveFormConsumer(
                    builder: (context, form, child) {
                      return Container(
                        width: Get.width * 1,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                elevation: 0.0,
                                primary: Colors.green,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  side:
                                      BorderSide(width: 0, color: Colors.white),
                                )),
                            child: Text('Actualizar Datos',
                                style: TextStyle(
                                  color: Colors.white,
                                )),
                            onPressed: form.valid
                                ? () => this.controller.updatePerson()
                                : null),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
