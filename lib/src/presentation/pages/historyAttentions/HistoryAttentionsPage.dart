import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/presentation/pages/historyAttentions/HistoryAttentionsController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:practics_movil/src/presentation/widgets/listAttention/ListAttentionWidget.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawer.dart';

class HistoryAttentionsPage extends GetView<HistoryAttentionsController> {
  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => HistoryAttentionsController());
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Historial de atenciones',
        icons: [
          Padding(
            padding: EdgeInsets.only(right: 10),
            child: IconButton(
                icon: Icon(Icons.calendar_today, color: Colors.blue,),
                onPressed: () async {
                  final newDateTime = await showRoundedDatePicker(
                    context: context,
                    initialDate: Jiffy(this.controller.date.value).dateTime,
                    firstDate: DateTime(2020, 7),
                    lastDate: Jiffy(this.controller.initialDate.value).dateTime,
                    borderRadius: 16,
                    barrierDismissible: false,
                    height: Get.height * 0.4,
                    // description: 'Si quiere saber que atenciones realizó cierto día por favor revise el calendario y seleccione una fecha en especifica.'
                  );
                  if (!newDateTime.isNull) {
                    this.controller.date.value =
                        Jiffy(newDateTime).format('yyyy-MM-dd');
                    this.controller.getAttentions();
                  }

                  // final List<DateTime> picked = await DateRagePicker.showDatePicker(
                  //     context: context,
                  //     initialFirstDate: new DateTime.now(),
                  //     initialLastDate: (new DateTime.now()).add(new Duration(days: 7)),
                  //     firstDate: new DateTime(2020),
                  //     lastDate: new DateTime(2021)
                  // );
                  // if (picked != null && picked.length == 2) {
                  //     print(picked);
                  // }
                }),
          )
        ],
      ),
      body: WillPopScope(
          onWillPop: () async => false,
          // child: Obx(()=> ItemList(
          //   optionsItems: this.controller.attentions.value,
          //   paddingHorizotal: 15,
          //   paddingVertical: 20,
          //   )
          // )
          child: Container(
              width: Get.width * 1,
              margin: EdgeInsets.only(top: 25),
              child: Obx(
                () => SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Text(
                        this.controller.date.value,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      ListAttentionWidget()
                    ],
                  ),
                ),
              ))),
      drawer: MenuDrawer(),
    );
  }
}
