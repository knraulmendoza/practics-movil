import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/domain/entities/attention/AttentionEntity.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/services/AttentionService.dart';

class HistoryAttentionsController extends GetxController {
  final global = Get.find<GlobalsController>();
  RxString date = ''.obs;
  RxString initialDate = ''.obs;
  @override
  void onInit() {
    super.onInit();
    this.date.value = Jiffy(Jiffy().subtract(days: 1)).format('yyyy-MM-dd');
    this.initialDate.value =
        Jiffy(Jiffy().subtract(days: 1)).format('yyyy-MM-dd');
        print('historial');
  }

  @override
  void onReady() {
    super.onReady();
    getAttentions();
  }

  @override
  void onClose() {
    return super.onClose();
  }

  void getAttentions() async {
    this.global.attentions.value = [];
    LoadingDialog.showLoadingDialog();
    final resp = await attentionService.getAttentionByDate(this.date.value);
    if (resp.data.state) {
      (resp.data.data['attentionsByDate'] as Map)?.forEach((key, value) {
        this.global.attentions.value =
            (value as List).map((e) => AttentionEntity.fromJson(e)).toList();
      });
    } else
      this.global.attentions.value = [];
    Get.back();
  }
}
