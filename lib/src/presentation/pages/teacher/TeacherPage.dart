import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TeacherController extends GetView<TeacherController> {
  @override
  Widget build(BuildContext context) {
    Get.put(new TeacherController());
    return Scaffold(
      appBar: AppBar(
        title: Text('Profesor'),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
    );
  }
}
