import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/presentation/pages/teachersByScenario/TeachersByScenarioController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';

class TeacherByScenarioPage extends GetView<TeacherByScenarioController> {
  @override
  Widget build(BuildContext context) {
    Get.put(new TeacherByScenarioController());
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Docentes',
        leading: IconButton(onPressed: () => Get.back(), icon: Icon(Icons.chevron_left, color: Colors.blue,)),
      ),
      body: Column(
        children: [
          Obx(
            () => Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: DropdownButton(
                  isExpanded: true,
                  hint: Text('Escenarios'),
                  value: this.controller.obj.value,
                  items: this
                      .controller
                      .scenarios
                      .value
                      .map((scenaio) => DropdownMenuItem(
                            child: Text(scenaio['name']),
                            value: scenaio['id'],
                          ))
                      .toList(),
                  onChanged: (scenario) async {
                    this.controller.obj.value = scenario;
                    await this.controller.getTeacher(scenario);
                  }),
            ),
          ),
          Divider(),
          Expanded(
              child: Obx(() => ListView.builder(
                  itemCount: this.controller.teachers.length,
                  itemBuilder: (_, i) {
                    return ListTile(
                      title: Text(this
                          .controller
                          .teachers
                          .value[i]
                          .personEntity
                          .fullName),
                      subtitle: Text(
                          'Telefono ${this.controller.teachers.value[i].personEntity.phone}'),
                      onTap: () {
                        Get.toNamed('/teacher-by-document', arguments: {
                          'document': this
                              .controller
                              .teachers
                              .value[i]
                              .personEntity
                              .document
                        });
                      },
                    );
                  })))
        ],
      ),
    );
  }
}
