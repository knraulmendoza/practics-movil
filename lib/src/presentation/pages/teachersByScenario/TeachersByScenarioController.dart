import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/teacher/TeacherEntity.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/services/PersonService.dart';
import 'package:practics_movil/src/services/RotationService.dart';

class TeacherByScenarioController extends GetxController {
  RxList scenarios = [].obs;
  RxList<TeacherEntity> teachers = <TeacherEntity>[].obs;
  RxInt obj = 0.obs;
  @override
  void onReady() async {
    super.onReady();
    final resp = await rotationService.scenarios();
    if (resp.data.state) {
      this.scenarios.value = (resp.data.data['scenarios'] as List)
          .map((scenaio) => {'id': scenaio['id'], 'name': scenaio['name']})
          .toList();
      this.obj.value = this.scenarios.value[0]['id'] as int;
      await getTeacher(this.obj.value);
    }
  }

  getTeacher(int scenarioId) async {
    this.teachers.value = [];
    LoadingDialog.showLoadingDialog(text: 'Cargando estudiantes ...');
    final resp = await personService.teacherByScenario(scenarioId);
    Get.back();
    if (resp.data.state) {
      this.teachers.value = (resp.data.data['teachers'] as List)
          .map((teacher) => new TeacherEntity.fromJson(teacher))
          .toList();
    }
  }
}
