import 'dart:convert';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:practics_movil/src/domain/entities/securityQuestion/SecurityQuestion.dart';
import 'package:practics_movil/src/services/LoginService.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';

class ChangePasswordController extends GetxController {
  RxBool iconVisibility = false.obs;
  RxList<SecurityQuestionEntity> questions = <SecurityQuestionEntity>[].obs;
  final form = FormGroup({
    'newPassword': FormControl(
      validators: [
        Validators.required,
      ],
    ),
    'repeatNewPassword': FormControl(validators: [
      Validators.required,
    ]),
    'question': FormControl<int>(validators: [
      Validators.required,
    ]),
    'answer': FormControl<String>(
      validators: [Validators.required],
    ),
  });
  @override
  void onInit() {
    super.onInit();
    this.showQuestions();
  }

  void showQuestions() async {
    final resp = await loginService.securityQuestions();
    if (resp.data.state) {
      this.questions.value = (resp.data.data['security_questions'] as List)
          .map((question) => new SecurityQuestionEntity.fromJson(question))
          .toList();
    }
  }

  changeNewPassword() async {
    if (this.form.value["newPassword"] ==
        this.form.value["repeatNewPassword"]) {
      LoadingDialog.showLoadingDialog(text: 'Cambiando contraseña');
      final resp = await loginService.updatedPassword(
          this.form.value["newPassword"], this.form.value["answer"],
          securityQuestionId: this.form.value["question"]);
      if (resp.data.state) {
        final currentUser = GetStorage().read('currentUser') != null
            ? jsonDecode(GetStorage().read('currentUser'))
            : null;
        currentUser['state_password'] = '1';
        GetStorage().write('currentUser', jsonEncode(currentUser));
        Get.back();
        Get.toNamed('/');
      } else {
        Get.back();
      }
    } else {
      ResponseWidget.snackbar(
          title: 'Error',
          msg: 'Las contraseñas no coinciden',
          type: TypeDialog.ERROR);
    }
  }
}
