import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/presentation/pages/changePassword/ChangePasswordController.dart';
import 'package:reactive_forms/reactive_forms.dart';

class ChangePasswordPage extends GetView<ChangePasswordController> {
  @override
  Widget build(BuildContext context) {
    Get.put(ChangePasswordController());
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Center(
          child: Card(
            elevation: 10,
            margin: EdgeInsets.all(20),
            child: Container(
              width: Get.width * 1,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
              child: ReactiveForm(
                formGroup: this.controller.form,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Ingrese una nueva contraseña',
                      style: TextStyle(fontSize: 28),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Obx(() => Theme(
                          data: ThemeData(primaryColor: Colors.green),
                          child: ReactiveTextField(
                            formControlName: 'newPassword',
                            obscureText: !this.controller.iconVisibility.value,
                            validationMessages: (control) => {
                              ValidationMessage.required:
                                  'El usuario es obligatorio',
                            },
                            decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(8.0),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                hintText: 'Nueva Contraseña',
                                hintStyle: TextStyle(
                                    color: Color.fromRGBO(0, 147, 57, 1)),
                                prefixIcon: Icon(Icons.lock),
                                suffixIcon: Obx(() => IconButton(
                                    icon: Icon(
                                        this.controller.iconVisibility.value
                                            ? Icons.visibility
                                            : Icons.visibility_off),
                                    onPressed: () {
                                      this.controller.iconVisibility.value =
                                          !this.controller.iconVisibility.value;
                                    }))),
                            textInputAction: TextInputAction.next,
                          ),
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Obx(() => Theme(
                          data: ThemeData(primaryColor: Colors.green),
                          child: ReactiveTextField(
                            formControlName: 'repeatNewPassword',
                            obscureText: !this.controller.iconVisibility.value,
                            decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(8.0),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                hintText: 'Repetir Nueva Contraseña',
                                hintStyle: TextStyle(
                                    color: Color.fromRGBO(0, 147, 57, 1)),
                                prefixIcon: Icon(Icons.lock),
                                suffixIcon: Obx(() => IconButton(
                                    icon: Icon(
                                        this.controller.iconVisibility.value
                                            ? Icons.visibility
                                            : Icons.visibility_off),
                                    onPressed: () {
                                      this.controller.iconVisibility.value =
                                          !this.controller.iconVisibility.value;
                                    }))),
                            validationMessages: (control) => {
                              ValidationMessage.required:
                                  'Este campo es obligatorio',
                            },
                            textInputAction: TextInputAction.next,
                          ),
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(
                      color: Colors.black,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Obx(
                      () => Container(
                        margin: EdgeInsets.symmetric(horizontal: 5),
                        child: ReactiveDropdownField<int>(
                            formControlName: 'question',
                            hint: Text(
                              'Preguntas de seguridad',
                              style: TextStyle(
                                  color: Color.fromRGBO(0, 147, 57, 1)),
                            ),
                            isExpanded: true,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(8.0),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              hintStyle: TextStyle(
                                  color: Color.fromRGBO(0, 147, 57, 1)),
                            ),
                            items: this
                                .controller
                                .questions
                                .map((question) => DropdownMenuItem(
                                      value: question.id,
                                      child: FittedBox(
                                        child: Text(question.question),
                                        fit: BoxFit.contain,
                                      ),
                                    ))
                                .toList()),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Theme(
                      data: ThemeData(primaryColor: Colors.green),
                      child: ReactiveTextField(
                        formControlName: 'answer',
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(10),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          hintText: 'Respuesta',
                          hintStyle:
                              TextStyle(color: Color.fromRGBO(0, 147, 57, 1)),
                        ),
                        validationMessages: (control) => {
                          ValidationMessage.required:
                              'Este campo es obligatorio',
                        },
                      ),
                    ),
                    ReactiveFormConsumer(
                      builder: (context, form, child) {
                        return Container(
                          width: Get.width * 1,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.green,
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                  ),
                                  elevation: 0.0),
                              child: Text('Cambiar contraseña',
                                  style: TextStyle(
                                    color: Colors.white,
                                  )),
                              onPressed: form.valid
                                  ? () => this.controller.changeNewPassword()
                                  : null),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
