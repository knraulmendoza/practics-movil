import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/domain/entities/event/EventEntity.dart';
import 'package:practics_movil/src/presentation/pages/home/HomeController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawer.dart';

class HomePage extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    Get.put(HomeController());
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBarWidget(icons: [IconButton(
                icon: Icon(Icons.refresh, color: Colors.blue,),
                onPressed: () => controller.getEvents(refresh: true))], title: 'Eventos',),
        body: Obx(() => !controller.connection.value
            ? Center(
                child: Text('No tiene conexión'),
              )
            : PagedListView<int, EventEntity>(
                pagingController: controller.pagingController,
                builderDelegate: PagedChildBuilderDelegate<EventEntity>(
                  itemBuilder: (context, item, index) => Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Card(
                      elevation: 10,
                      child: InkWell(
                        onTap: () {
                          Get.toNamed('/event', arguments: item);
                        },
                        splashColor: Colors.blue,
                        child: Container(
                            height: Get.height * 0.21,
                            padding: EdgeInsets.all(10),
                            child: Row(
                              children: <Widget>[
                                Container(
                                    width: Get.width * 0.3,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        Image.network(
                                          item.image,
                                          width: 110,
                                          height: 110,
                                          errorBuilder: (context, error, stackTrace) {
                                            return Container(
                                              color: Colors.amber,
                                              alignment: Alignment.center,
                                              child: const Text(
                                                'Whoops!',
                                                style: TextStyle(fontSize: 30),
                                              ),
                                            );
                                          },
                                          loadingBuilder:
                                              (context, child, progress) {
                                            return progress == null
                                                ? child
                                                : Center(
                                                    child:
                                                        CircularProgressIndicator(
                                                    strokeWidth: 2.0,
                                                  ));
                                          },
                                        ),
                                        Text(
                                            '${Jiffy(item.date).format('yyyy-MM-dd HH:mm')}')
                                      ],
                                    )),
                                Container(
                                  width: Get.width * 0.55,
                                  padding: EdgeInsets.only(left: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        item.title,
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Container(
                                        height: Get.height * 0.13,
                                        child: HtmlWidget(item.description),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      ),
                    ),
                  ),
                ),
              )),
        drawer: MenuDrawer(),
      ),
    );
  }
}
