import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:practics_movil/src/domain/entities/event/EventEntity.dart';
import 'package:practics_movil/src/helpers/Connection.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/services/EventService.dart';

class HomeController extends GetxController {
  RxList<EventEntity> events = <EventEntity>[].obs;
  RxBool connection = true.obs;

  PagingController<int, EventEntity> pagingController =
      PagingController(firstPageKey: 1);

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onInit() {
    this.pagingController.addPageRequestListener((pageKey) {
      getEvents(pageKey: pageKey);
    });
    super.onInit();
  }

  Future<void> getEvents({int pageKey, bool refresh = false}) async {
    try {
      if (refresh) {
        this.pagingController.refresh();
      }
      final resp = await eventService.getEvents(pageKey);
      if (resp.data.state) {
        final eventsAux = (resp.data.data['events'] as List)
            .map((event) => EventEntity.fromJson(event))
            .toList();
        if (eventsAux.length < 0) {
          this.pagingController.appendLastPage(eventsAux);
        } else {
          pageKey += 1;
          this.pagingController.appendPage(eventsAux, pageKey);
        }
      }
    } catch (error) {
      this.pagingController.error = error;
    }
  }
}
