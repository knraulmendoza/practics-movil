import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/rotation/RotationEntity.dart';
import 'package:practics_movil/src/domain/entities/student/StudentEntity.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/services/GroupStudentService.dart';
import 'package:practics_movil/src/services/RotationService.dart';

class RotationController extends GetxController {
  RxList<RotationEntity> rotations = <RotationEntity>[].obs;
  RxList<StudentEntity> students = <StudentEntity>[].obs;
  final globalController = Get.find<GlobalsController>();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() async {
    super.onReady();
    LoadingDialog.showLoadingDialog(text: 'Cargando Rotaciones y estudiantes');
    await getRotations();
    await getStudentGroup();
    Get.back();
  }

  Future<void> getRotations() async {
    final resp =
        await rotationService.getRotations(this.globalController.group.id);
    if (resp.data.state) {
      this.rotations.value = (resp.data.data['rotations'] as List)
          .map((rotation) => RotationEntity.fromJson(rotation))
          .toList();
    }
  }

  Future<void> getStudentGroup() async {
    final resp = await groupStudentService
        .getStudentGroup(this.globalController.group.id);
    if (resp.data.state) {
      this.students.value = (resp.data.data['studentsGroup'] as List)
          .map((student) => StudentEntity.fromJson(student))
          .toList();
    }
  }
}
