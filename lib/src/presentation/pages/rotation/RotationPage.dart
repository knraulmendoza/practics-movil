import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/presentation/pages/rotation/RotationController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawer.dart';

class RotationPage extends GetView<RotationController> {
  const RotationPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(RotationController());
    return Scaffold(
      appBar: AppBarWidget(title: 'Rotaciones'),
      body: Container(
          padding: EdgeInsets.all(10),
          height: Get.height * 1,
          child: ListView(
            children: <Widget>[
              Text('Rotaciones del semestre',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              Obx(() => this.controller.rotations.length == 0
                  ? Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        'No estas asociado a ninguna rotación',
                        textAlign: TextAlign.center,
                      ),
                    )
                  : Column(
                      mainAxisSize: MainAxisSize.min,
                      children: List.generate(
                          this.controller.rotations.length,
                          (i) => ListTile(
                                leading: Text((i + 1).toString()),
                                title: Text(this.controller.rotations[i].stage),
                                subtitle: Text(
                                    '${this.controller.rotations[i].area} (${this.controller.rotations[i].workinDay})'),
                                trailing: Icon(
                                    this.controller.rotations[i].state
                                        ? Icons.check_circle
                                        : Icons.check_circle_outline,
                                    color: Colors.green),
                              )),
                    )),
              Divider(),
              Text('Estudiantes del grupo',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              Obx(() => this.controller.students.length == 0
                  ? Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        'No estas asociado a ningún grupo',
                        textAlign: TextAlign.center,
                      ),
                    )
                  : Column(
                      mainAxisSize: MainAxisSize.min,
                      children: List.generate(
                          this.controller.students.length,
                          (i) => ListTile(
                                leading: Text((i + 1).toString()),
                                title: Text(this
                                    .controller
                                    .students[i]
                                    .personEntity
                                    .fullName),
                              )),
                    )),
            ],
          )),
      drawer: MenuDrawer(),
    );
  }
}
