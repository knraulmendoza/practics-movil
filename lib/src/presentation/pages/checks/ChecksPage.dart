import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/answer/AnswerEntity.dart';
import 'package:practics_movil/src/domain/enums/RolEnum.dart';
import 'package:practics_movil/src/presentation/pages/checks/ChecksController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';

class ChecksPage extends GetView<ChecksController> {
  // TextEditingController myController = TextEditingController();
  final data = Get.arguments;
  @override
  Widget build(BuildContext context) {
    Get.put(ChecksController());
    return Scaffold(
      appBar: AppBarWidget(
        title: 'chequeo',
        leading: IconButton(onPressed: () => Get.back(), icon: Icon(Icons.chevron_left, color: Colors.blue,)),
      ),
      body: ListView(
        // mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text(
              '${(data['rol'] as int) == RolEnum.STUDENT ? 'Estudiante' : 'Docente'} ${data['name']}',
              textAlign: TextAlign.center),
          ListTile(
            title: Text(
              'Preguntas',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  'Respuestas',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                  'obser.',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Obx(() => SingleChildScrollView(
                child: Column(
                    children: List.generate(controller.questions.length, (i) {
                  return ListTile(
                    title: Text(controller.questions[i].question.toLowerCase()),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Checkbox(
                          value:
                              controller.answers[i].answer == 1 ? true : false,
                          onChanged: (value) =>
                              controller.answers[i].answer = value ? 1 : 0,
                          activeColor: Colors.blue[400],
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        IconButton(
                            icon: Icon(Icons.edit),
                            onPressed: () =>
                                observationAnswers(controller.answers[i]))
                      ],
                    ),
                  );
                })),
              ))
        ],
      ),
      bottomNavigationBar: Row(
        children: <Widget>[
          Expanded(
              child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.blue[400],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                  ),
                ),
                child: Text('Observación del chequeo'),
                onPressed: () => observationCheck()),
          )),
        ],
      ),
    );
  }

  void observationAnswers(AnswerEntity answerEntity) {
    controller.observationController.value.text =
        answerEntity.observation ?? '';
    Get.defaultDialog(
      radius: 20,
      title: 'Observación',
      content: Column(
        children: <Widget>[
          Container(
            height: Get.height * 0.2,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: Colors.grey, width: 1)),
            child: Obx(() => TextField(
                  decoration: InputDecoration(alignLabelWithHint: false),
                  maxLines: 5,
                  controller: controller.observationController.value,
                  onChanged: (value) => answerEntity.observation = value,
                )),
          ),
        ],
      ),
      textConfirm: 'Guardar',
      textCancel: answerEntity.observation == null ? 'Cancelar' : 'Limpiar',
      onCancel: () => answerEntity.observation = null,
      onConfirm: () => Get.back(),
    );
  }

  void observationCheck() {
    controller.observationController.value.text =
        controller.checkEntity.value.observation ?? '';
    Get.defaultDialog(
        radius: 20,
        title: 'Observación',
        content: Column(
          children: <Widget>[
            Container(
              height: Get.height * 0.2,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(color: Colors.grey, width: 1)),
              child: Obx(() => TextField(
                    decoration: InputDecoration(alignLabelWithHint: false),
                    maxLines: 5,
                    controller: controller.observationController.value,
                    onChanged: (value) =>
                        controller.checkEntity.value.observation = value,
                  )),
            ),
          ],
        ),
        textConfirm: 'Guardar',
        textCancel: controller.checkEntity.value.observation == null
            ? 'Cancelar'
            : 'Limpiar',
        onCancel: () => controller.checkEntity.value.observation = null,
        onConfirm: () async {
          Get.back();
          await controller.saveCheck(data['id']);
          if (this.controller.showModal.value) chequeo();
        });
  }

  void chequeo() {
    Get.defaultDialog(
        radius: 20,
        title: 'Chequeo',
        content: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
                child: Text('Consultar'),
                onPressed: () {
                  Get.toNamed('/comparatorCheck', arguments: {
                    'evaluated': this.controller.checkEntity.value.evaluated,
                    'name': data['name']
                  });
                }),
            SizedBox(
              width: 10,
            ),
            ElevatedButton(
                child: Text('Volver'),
                onPressed: () {
                  Get.toNamed('/students');
                }),
          ],
        ));
  }
}
