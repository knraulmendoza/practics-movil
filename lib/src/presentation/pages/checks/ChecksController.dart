import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:practics_movil/src/domain/entities/answer/AnswerEntity.dart';
import 'package:practics_movil/src/domain/entities/check/CheckEntity.dart';
import 'package:practics_movil/src/domain/entities/person/PersonEntity.dart';
import 'package:practics_movil/src/domain/entities/question/QuestionEntity.dart';
import 'package:practics_movil/src/domain/enums/RolEnum.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:practics_movil/src/services/AnswerService.dart';
import 'package:practics_movil/src/services/CheckService.dart';
import 'package:practics_movil/src/services/QuestionService.dart';

class ChecksController extends GetxController {
  final RxList<QuestionEntity> questions = <QuestionEntity>[].obs;
  final RxList<AnswerEntity> answers = <AnswerEntity>[].obs;
  final data = Get.arguments;
  final Rx<TextEditingController> observationController =
      TextEditingController().obs;
  final Rx<CheckEntity> checkEntity =
      CheckEntity(evaluated: null, evaluator: null, date: null).obs;
  final global = Get.find<GlobalsController>();
  RxBool showModal = false.obs;
  @override
  void onReady() {
    super.onReady();
    getQuestions();
  }

  getQuestions() async {
    LoadingDialog.showLoadingDialog(text: 'Cargando preguntas ...');
    final resp = await questionService.getQuestion();
    if (resp.data.state) {
      this.questions.value = (resp.data.data['questions'] as List)
          .map((question) => QuestionEntity.fromJson(question))
          .toList();
      this.answers.value = (resp.data.data['questions'] as List)
          .map(
              (question) => AnswerEntity(questionId: question['id'], answer: 0))
          .toList();
    }
    Get.back();
  }

  Future<void> saveCheck(int evaluated) async {
    final person = GetStorage().read('person');
    final pEntity = PersonEntity.fromJsonSql(jsonDecode(person));
    this.checkEntity.value.evaluated = evaluated;
    this.checkEntity.value.evaluator = pEntity.id;
    LoadingDialog.showLoadingDialog(text: 'Guardando Chequeo');
    final respCheck = await checkService.save(checkEntity.value,
        action: (this.data['rol'] as int) == RolEnum.STUDENT ? 1 : 2);
    Get.back();
    if (respCheck.data.state) {
      LoadingDialog.showLoadingDialog(text: 'Guardando Respuestas');
      final respAnswers = await answerService.save(answers, evaluated);
      Get.back();
      if (respAnswers.data.state) {
        if((this.data['rol'] as int) == RolEnum.STUDENT){
            this
            .global
            .studentsGroup
            .value
            .firstWhere((student) => student.personEntity.id == evaluated,
                orElse: null)
            ?.checks = 1;
        }
        
        ResponseWidget.snackbar(
            title: 'Respuestas',
            msg: 'Se registrarón las respuesta de manera correcta',
            type: TypeDialog.OK);
        this.showModal.value = true;
      } else {
        ResponseWidget.snackbar(
            title: 'Respuestas',
            msg: respAnswers.data.message,
            type: TypeDialog.ERROR);
      }
    } else {
      ResponseWidget.snackbar(
          title: 'Chequeo',
          msg: respCheck.data.message,
          type: TypeDialog.ERROR);
    }
  }

  saveAnswers() {}
}
