import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:practics_movil/src/presentation/pages/attention/AttentionController.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawer.dart';
import 'package:practics_movil/src/presentation/widgets/listAttention/ListAttentionWidget.dart';

class AttentionPage extends GetView<AttentionController> {
  @override
  Widget build(BuildContext context) {
    Get.put(AttentionController());
    // this.controller.getAttentions();
    return WillPopScope(
      onWillPop: () async => false,
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              'Atención ${Jiffy().format('yyyy-MM-dd')}',
              style: TextStyle(fontSize: 18),
            ),
            bottom: TabBar(
              tabs: [
                Tab(
                  child: Text(
                    'registrar',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Tab(
                  child: Obx(() => Text(
                      'Atenciones ${controller.globalController.attentions.value.length}',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                )
              ],
            ),
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.refresh),
                  onPressed: () async => await this.controller.getAttentions()),
              IconButton(
                  icon: Icon(Icons.send_and_archive),
                  onPressed: () => this.showAttentionSent()),
            ],
          ),
          body: TabBarView(
            children: [this.addAttention(), ListAttentionWidget()],
          ),
          drawer: MenuDrawer(),
        ),
      ),
    );
  }

  void showAttentionSent() {
    this.controller.getAttentionsNoSent();
    Get.defaultDialog(
      radius: 20,
      title: "Atenciones a sincronizar",
      content: Container(
          height: Get.height * 0.3,
          width: 260,
          child: this.controller.attentionsNoSent.length == 0
              ? Center(
                  child: Text('No hay datos a sincronizar'),
                )
              : ListView.builder(
                  itemCount: this.controller.attentionsNoSent.length,
                  itemBuilder: (_, index) {
                    return ListTile(
                      title: Text(this
                          .controller
                          .attentionsNoSent[index]
                          .patientEntity
                          .name),
                      trailing: Obx(() => Icon(
                          this.controller.attentionsNoSent[index].sent
                              ? Icons.check_circle
                              : Icons.check_circle_outline)),
                    );
                  })),
      confirm: Container(
        width: Get.width * 0.6,
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.green,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30)),
              ),
            ),
            child: Text('Enviar atenciones'),
            onPressed: this.controller.attentionsNoSent.length != 0
                ? () => this.controller.sentData()
                : null),
      ),
    );
  }

  Widget addAttention() {
    return Container(
      child: Form(
        child: ListView(
          padding: EdgeInsets.all(10),
          children: <Widget>[
            Obx(() => DropdownButtonFormField(
                  value: this
                      .controller
                      .attentionEntity
                      .value
                      .patientEntity
                      .documentType,
                  items: [
                    DropdownMenuItem(child: Text('Cedula'), value: 1),
                    DropdownMenuItem(
                      child: Text('Tarjeta de identidad'),
                      value: 2,
                    )
                  ],
                  onChanged: (value) => this
                      .controller
                      .attentionEntity
                      .value
                      .patientEntity
                      .documentType = value,
                  decoration: InputDecoration(labelText: 'Tipo de documento'),
                )),
            Theme(
              data: ThemeData(primaryColor: Colors.blue),
              child: TextFormField(
                controller: controller.controllerDocument,
                onChanged: (value) => this
                    .controller
                    .attentionEntity
                    .value
                    .patientEntity
                    .document = value,
                keyboardType: TextInputType.number,
                maxLength: 11,
                decoration: InputDecoration(
                    hintText: 'Documento', labelText: 'Documento'),
              ),
            ),
            Theme(
              data: ThemeData(primaryColor: Colors.blue),
              child: TextFormField(
                controller: controller.controllerName,
                onChanged: (value) => this
                    .controller
                    .attentionEntity
                    .value
                    .patientEntity
                    .name = value,
                decoration: InputDecoration(labelText: 'Nombre completo'),
              ),
            ),
            Obx(() => MultiSelectFormField(

                  initialValue:
                      this.controller.attentionEntity.value.proceduresId,
                  title: Text('Procedimientos'),
                  autovalidate: AutovalidateMode.onUserInteraction,
                  validator: (value) {
                    if (value == null || value.length == 0) {
                      return 'Selecciona un procedimiento';
                    }
                    return '';
                  },
                  dataSource: this
                      .controller
                      .procedures
                      .map((procedure) =>
                          {"display": procedure.name, "value": procedure.id})
                      .toList(),
                  textField: 'display',
                  valueField: 'value',
                  required: true,
                  okButtonLabel: 'Ok',
                  cancelButtonLabel: 'Cancelar',
                  hintWidget: Text('Seleccione el procedimiento aplicado'),
                  onSaved: (value) {
                    this.controller.attentionEntity.value.procedures = [];
                    this.controller.attentionEntity.value.proceduresId = [];
                    if (value.length == 0) return;
                    this.controller.attentionEntity.value.proceduresId =
                        List<int>.from(value);
                    List<int>.from(value).forEach((val) {
                      this.controller.attentionEntity.value.procedures.add(this
                          .controller
                          .procedures
                          .firstWhere((procedure) => procedure.id == val));
                    });
                  },
                )),
            SizedBox(
              height: 10,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.blue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
              ),
              onPressed: () => this.controller.addAttention(),
              child: Text('Guardar Atención'),
            ),
          ],
        ),
      ),
    );
  }
}
