import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/data/service/AttentionSerData.dart';
import 'package:practics_movil/src/domain/entities/attention/AttentionEntity.dart';
import 'package:practics_movil/src/domain/entities/patient/PatientEntity.dart';
import 'package:practics_movil/src/domain/entities/procedure/ProcedureEntity.dart';
import 'package:practics_movil/src/helpers/Connection.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/services/AttentionService.dart';

class AttentionController extends GetxController {
  Rx<AttentionEntity> attentionEntity = AttentionEntity(
          date: null,
          procedures: [],
          proceduresId: [],
          patientEntity: new PatientEntity(document: null, name: null))
      .obs;
  RxList<ProcedureEntity> procedures = <ProcedureEntity>[].obs;
  RxList<AttentionEntity> attentions = <AttentionEntity>[].obs;
  RxList<AttentionEntity> attentionsNoSent = <AttentionEntity>[].obs;
  final globalController = Get.find<GlobalsController>();
  TextEditingController controllerDocument = TextEditingController();
  TextEditingController controllerName = TextEditingController();
  @override
  void onReady() async {
    super.onReady();
    await getProcedures();
    await getAttentions();
  }

  @override
  void onClose() {
    this.globalController.attentions.value = [];
    return super.onClose();
  }

  void getProcedures() async {
    LoadingDialog.showLoadingDialog(text: 'Cargando procedimientos ...');
    final resp = await attentionService.getProcedures();
    Get.back();
    if (resp.data.state) {
      this.procedures.value = (resp.data.data['procedures'] as List)
          .map((procedure) => ProcedureEntity.fromJson(procedure))
          .toList();
    }
  }

  void deleting() async {
    await attentionSerData.deleting();
  }

  void getAttentions() async {
    if (this.globalController.connection.value) {
      LoadingDialog.showLoadingDialog(text: 'Cargando atenciones ...');
      final resp = await attentionService.getAttention();
      Get.back();
      if (resp.data.state) {
        (resp.data.data['attentions'] as List)
            .asMap()
            .forEach((i, attention) async {
          String document =
              AttentionEntity.fromJson(attention).patientEntity.document;
          if (attentionSerData.getAttentionIndex(document) != -1) {
            attentionSerData.updateAttention(
                attentionSerData.getAttentionIndex(document),
                AttentionEntity.fromJson(attention).state);
          } else {
            await attentionSerData
                .add(AttentionEntity.fromJson(attention).toObjectHive());
          }
        });
      }
    }
    final attentions = attentionSerData.getAllDateNow();
    print('attentions => ${attentions.length}');
    if (attentions.isNotEmpty) {
      this.globalController.attentions.value = attentions
          .map((e) => AttentionEntity.fromObjectHive(e))
          .toList()
          .reversed
          .toList();
    } else {
      this.globalController.attentions.value = [];
    }
  }

  void getAttentionsNoSent() {
    this.attentionsNoSent.value = attentionSerData
        .getAllNoSent()
        .map((attention) => AttentionEntity.fromObjectHive(attention))
        .toList()
        .reversed
        .toList();
  }

  void sentData() async {
    if (this.globalController.connection.value) {
      if (await Connection.connection) {
        LoadingDialog.showLoadingDialog(text: 'Enviando atenciones');
        this.attentionsNoSent.asMap().forEach((index, attention) async {
          final resp = await attentionService.addAttention(attention);
          if (resp.data.state) {
            attentionSerData.getAttention(index).stateSent = true;
            attentionSerData.getAttention(index).stateSent = true;
            attentionSerData.getAttention(index).save();
            this.attentionsNoSent[index].sent = true;
          } else {
            ResponseWidget.snackbar(
                title: 'Registro de atención',
                msg: resp.data.message,
                type: TypeDialog.ERROR);
          }
        });
        Get.back();
      } else {
        ResponseWidget.snackbar(
            title: 'Conexión', msg: 'Sin conexión', type: TypeDialog.ERROR);
      }
    } else {
      ResponseWidget.snackbar(
          title: 'Conexión',
          msg: 'Debes habilitar la conexión en el menú',
          type: TypeDialog.ERROR);
    }
  }

  void addAttention() async {
    if (this.attentionEntity.value.patientEntity.documentType != null &&
        this.attentionEntity.value.patientEntity.document != null &&
        this.attentionEntity.value.patientEntity.name != null &&
        this.attentionEntity.value.procedures.length != 0) {
      LoadingDialog.showLoadingDialog(text: 'Guardando atención ...');
      final respLocal =
          await attentionSerData.add(this.attentionEntity.value.toObjectHive());
      Get.back();
      if (respLocal.state) {
        // getAttentions();
        this.globalController.attentions.add(this.attentionEntity.value);
        this.globalController.attentions.value =
            this.globalController.attentions.reversed.toList();

        ResponseWidget.snackbar(
            title: 'Atención (Local)', msg: 'Se registro ok', type: TypeDialog.OK);
      } else {
        ResponseWidget.snackbar(
            title: 'Atención (Local)', msg: respLocal.message, type: TypeDialog.ERROR);
      }
      if (this.globalController.connection.value) {
        if (await Connection.connection) {
          final resp =
              await attentionService.addAttention(this.attentionEntity.value);
          if (resp.data.state) {
            attentionSerData.last().stateSent = true;
            attentionSerData.last().save();
          }
          // } else {
          //   ResponseWidget.snackbar(
          //       title: 'Registro de atención',
          //       msg: resp.data.message,
          //       type: TypeDialog.ERROR);
          // }
        } else {
          ResponseWidget.snackbar(
              title: 'Conexión', msg: 'Sin conexión', type: TypeDialog.ERROR);
        }
      }
      this.controllerDocument.clear();
      this.controllerName.clear();
      this.attentionEntity.value = new AttentionEntity(
          date: null,
          procedures: this.attentionEntity.value.procedures,
          proceduresId: this.attentionEntity.value.proceduresId,
          patientEntity: new PatientEntity(document: null, name: null));
    } else {
      ResponseWidget.snackbar(
          title: 'Error Registrar la atención',
          msg: 'Debe llenar todos los campos',
          type: TypeDialog.ERROR);
    }
  }
}
