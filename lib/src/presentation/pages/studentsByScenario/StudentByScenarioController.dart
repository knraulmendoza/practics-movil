import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/student/StudentEntity.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/services/PersonService.dart';
import 'package:practics_movil/src/services/RotationService.dart';
import 'package:practics_movil/src/services/subjectService.dart';

class StudentByScenarioController extends GetxController {
  RxList scenarios = [].obs;
  RxList<StudentEntity> students = <StudentEntity>[].obs;
  RxInt scenarioId = 0.obs;
  RxInt subjectId = 0.obs;
  RxList subjects = [].obs;
  @override
  void onReady() async {
    super.onReady();
    await initial();
  }

  initial () async {
    final resp = await rotationService.scenarios();
    if (resp.data.state) {
      this.scenarios.value = (resp.data.data['scenarios'] as List)
          .map((scenaio) => {'id': scenaio['id'], 'name': scenaio['name']})
          .toList();
      this.scenarioId.value = this.scenarios.value[0]['id'] as int;
      await getStudent();
    }

    final respSubject = await subjectService.subjects();
        if (respSubject.data.state) {
      this.subjects.value = (respSubject.data.data['subjects'] as List)
          .map((subject) => {'id': subject['id'], 'name': subject['name']})
          .toList();
      this.subjects.add({ 'id': null, 'name': 'Ninguno'});
      this.subjectId.value = this.subjects.value[this.subjects.length-1]['id'] as int;
    }
  }

  getStudent() async {
    this.students.value = [];
    LoadingDialog.showLoadingDialog(text: 'Cargando estudiantes ...');
    final resp = await personService.studentByScenario(this.scenarioId.value, subjecId: this.subjectId.value);
    Get.back();
    if (resp.data.state) {
      this.students.value = (resp.data.data['students'] as List)
          .map((student) => new StudentEntity.fromJson(student))
          .toList();
    }
  }
}
