import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/presentation/pages/studentsByScenario/StudentByScenarioController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawer.dart';

class StudentByScenarioPage extends GetView<StudentByScenarioController> {
  @override
  Widget build(BuildContext context) {
    Get.put(new StudentByScenarioController());
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Estudiantes'
      ),
      body: Column(
        children: [
          Obx(
            () => Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: DropdownButton(
                  isExpanded: true,
                  hint: Text('Escenarios'),
                  value: this.controller.scenarioId.value,
                  items: this
                      .controller
                      .scenarios
                      .value
                      .map((scenaio) => DropdownMenuItem(
                            child: Text(scenaio['name']),
                            value: scenaio['id'],
                          ))
                      .toList(),
                  onChanged: (scenario) async {
                    this.controller.scenarioId.value = scenario;
                    await this.controller.getStudent();
                  }),
            ),
          ),
                    Obx(
            () => Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: DropdownButton(
                  isExpanded: true,
                  hint: Text('Asignaturas'),
                  value: this.controller.subjectId.value,
                  items: this
                      .controller
                      .subjects
                      .value
                      .map((subject) => DropdownMenuItem(
                            child: Text(subject['name']),
                            value: subject['id'],
                          ))
                      .toList(),
                  onChanged: (subject) async {
                    this.controller.subjectId.value = subject;
                    await this.controller.getStudent();
                  }),
            ),
          ),
          Divider(),
          Expanded(
              child: Obx(() => ListView.builder(
                  itemCount: this.controller.students.length,
                  itemBuilder: (_, i) {
                    return ListTile(
                      title: Text(this
                          .controller
                          .students
                          .value[i]
                          .personEntity
                          .fullName),
                      subtitle: Text(
                          '${this.controller.students.value[i].attentions} atenciones durante el semestre'),
                    );
                  })))
        ],
      ),
      drawer: MenuDrawer(),
    );
  }
}
