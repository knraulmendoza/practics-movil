import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:practics_movil/src/domain/entities/answer/AnswerEntity.dart';
import 'package:practics_movil/src/domain/entities/person/PersonEntity.dart';
import 'package:practics_movil/src/domain/entities/poll/PollEntity.dart';
import 'package:practics_movil/src/domain/entities/question/QuestionEntity.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:practics_movil/src/services/AnswerService.dart';
import 'package:practics_movil/src/services/CheckService.dart';
import 'package:practics_movil/src/services/QuestionService.dart';

class EncuestaController extends GetxController {
  final data = Get.arguments;
  final RxList<QuestionEntity> questions = <QuestionEntity>[].obs;
  final RxList<AnswerEntity> answers = <AnswerEntity>[].obs;
  final Rx<PollEntity> pollEntity = PollEntity(attentionId: null).obs;
  @override
  void onReady() {
    super.onReady();
    getQuestions();
  }

  getQuestions() async {
    LoadingDialog.showLoadingDialog(text: 'Cargando preguntas ...');
    final resp =
        await questionService.questionEncuesta({"check_type": "", "type": "2"});
    if (resp.data.state) {
      this.questions.value = (resp.data.data['questions'] as List)
          .map((question) => QuestionEntity.fromJson(question))
          .toList();
      this.answers.value = (resp.data.data['questions'] as List)
          .map((question) =>
              AnswerEntity(questionId: question['id'], answer: null))
          .toList();
    }
    Get.back();
  }
  void modalMsg() {
    Get.defaultDialog(
      barrierDismissible: false,
        radius: 20,
        title: 'Encuesta',
        content: Column(
          children: [
            Text('Se registrarón las respuesta de manera correcta', textAlign: TextAlign.center,),
          ],
        ),
        actions: [
            ElevatedButton(
              child: Text('Aceptar'),
              onPressed: () {
                this.pollEntity.value = PollEntity(attentionId: null);
                Get.toNamed('/patient');
            }),
        ]
    );
  }

  saveEncuesta() async {
    LoadingDialog.showLoadingDialog(text: 'Guardando Encuesta ...');
    this.pollEntity.value.attentionId = this.data['attention_id'];
    final respPoll = await checkService.saveEncuesta(this.pollEntity.value);
    Get.back();
    if (respPoll.data.state) {
      LoadingDialog.showLoadingDialog(text: 'Guardando Respuestas ... ');
      final respAnswers = await answerService.saveAnswerEncuesta(
          answers, this.pollEntity.value.attentionId);
      Get.back();
      if (respAnswers.data.state) {
        ResponseWidget.snackbar(
            title: 'Respuestas',
            msg: 'Se registrarón las respuesta de manera correcta',
            type: TypeDialog.OK);
          this.modalMsg();

      } else {
        ResponseWidget.snackbar(
            title: 'Respuestas',
            msg: respAnswers.data.message,
            type: TypeDialog.ERROR);
      }
    } else {
      ResponseWidget.snackbar(
          title: 'Encuesta',
          msg: respPoll.data.message,
          type: TypeDialog.ERROR);
    }
  }
}
