import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/presentation/pages/encuesta/EncuestaController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';

enum BestTutorSite { UNO, DOS, TRES, CUATRO, CINCO }

class EncuestaPage extends GetView<EncuestaController> {
  @override
  Widget build(BuildContext context) {
    Get.put(EncuestaController());
    final _site = BestTutorSite.UNO;
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Encuesta paciente'
      ),
      backgroundColor: Colors.white,
      body: ListView(
        // padding: EdgeInsets.symmetric(horizontal: 10),
        // mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text(
            this.controller.data['name'],
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 30),
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      'uno = Nunca',
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(
                      'dos = Casi Nunca',
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(
                      'tres = Rara vez',
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      'cuatro = Casi siempre',
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(
                      'cinco = Siempre',
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
              ],
            ),
          ),
          ListTile(
            title: Text(
              'Preguntas',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              'El estudiante de enfermería:',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
          ),
          Obx(() => SingleChildScrollView(
                child: Column(
                    children: List.generate(controller.questions.length, (i) {
                  return ListTile(
                    title: Text(controller.questions[i].question.toLowerCase()),
                    subtitle: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: List.generate(5, (index) {
                          return Column(
                            children: [
                              Text('${index + 1}'),
                              Obx(
                                () => Radio(
                                    value: index + 1,
                                    groupValue:
                                        this.controller.answers[i].answer,
                                    onChanged: (answer) {
                                      this.controller.answers[i].answer =
                                          answer;
                                    }),
                              )
                            ],
                          );
                        })),
                  );
                })),
              ))
        ],
      ),
      bottomNavigationBar: Row(
        children: <Widget>[
          Expanded(
              child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Obx(
              () => ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blue[400],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                  ),
                  child: Text('Encuesta'),
                  onPressed: this
                          .controller
                          .answers
                          .value
                          .every((answer) => answer.answer != null)
                      ? () => this.controller.saveEncuesta()
                      : null),
            ),
          )),
        ],
      ),
    );
  }
}
