import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/data/service/AssistenceSerData.dart';
import 'package:practics_movil/src/domain/entities/asistent/AssistentEntity.dart';
import 'package:practics_movil/src/domain/enums/AssistEnum.dart';
import 'package:practics_movil/src/presentation/pages/Assistent/AssistentController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawer.dart';

class AssistentPage extends GetView<AssistentController> {
  const AssistentPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(AssistentController());
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Asistencia',
        icons: [
          IconButton(
              icon: Icon(Icons.send_and_archive, color: Colors.blue,),
              onPressed: () => this.controller.sentData())
        ],
      ),
      body: Obx(() => ListView.builder(
          itemCount: controller.global.studentsGroup.length,
          itemBuilder: (_, i) => ListTile(
                title: Text(
                    controller.global.studentsGroup[i].personEntity.fullName),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Obx(() => Checkbox(
                          value: controller.assistences[i].assist ==
                                  AssistenceEnum.YES
                              ? true
                              : false,
                          onChanged: (value) => controller
                                  .assistences[i].assist =
                              value ? AssistenceEnum.YES : AssistenceEnum.NOT,
                          activeColor: Colors.blue[400],
                        )),
                    SizedBox(
                      width: 10,
                    ),
                    IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () => observation(controller.assistences[i]))
                  ],
                ),
              ))),
      drawer: MenuDrawer(),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.check),
          onPressed: () => controller.checkAssistences()),
    );
  }

  void observation(AssistenceEntity assistence) {
    controller.observationController.value.text =
        assistence.observation ?? '';
    bool assist = assistence.assist == AssistenceEnum.YES ? true : false;
    Get.defaultDialog(
        radius: 20,
        title: 'Motivo',
        content: Column(
          children: <Widget>[
            Container(
              height: Get.height * 0.2,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(color: Colors.grey, width: 1)),
              child: TextField(
                decoration: InputDecoration(alignLabelWithHint: false),
                maxLines: 5,
                controller: controller.observationController.value,
                onChanged: (value) => assistence.observation = value,
              ),
            ),
            ListTile(
              title: Text('Excusa'),
              trailing: Obx(() => Radio(
                groupValue: assistence.assist,
                value: AssistenceEnum.EXCUSE,
                    onChanged: (value) {
                      assistence.assist = value;
                    },
                    activeColor: Colors.blue[400],
                  )),
            ),
            ListTile(
              title: Text('Covig negativo'),
              trailing: Obx(() => Radio(
                groupValue: assistence.assist,
                value: AssistenceEnum.COVIG_NEG,
                    onChanged: (value) {
                      assistence.assist = value;
                    },
                    activeColor: Colors.blue[400],
                  )),
            ),
            ListTile(
              title: Text('Covig Positivo'),
              trailing: Obx(() => Radio(
                groupValue: assistence.assist,
                value: AssistenceEnum.COVIG_POS,
                    onChanged: (value) {
                      assistence.assist = value;
                    },
                    activeColor: Colors.blue[400],
                  )),
            )
          ],
        ),
        textConfirm: 'Guardar',
        textCancel: 'Cancelar',
        // onCancel: () =>controller.checkEntity.value.observation = null,
        onConfirm: () {
          Get.back();
          // controller.saveCheck(student.student.value.personEntity.id);
        });
  }
}
