import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/data/service/AssistenceSerData.dart';
import 'package:practics_movil/src/domain/entities/asistent/AssistentEntity.dart';
import 'package:practics_movil/src/domain/enums/AssistEnum.dart';
import 'package:practics_movil/src/helpers/Connection.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:practics_movil/src/services/AssistenceService.dart';

class AssistentController extends GetxController {
  final global = Get.find<GlobalsController>();
  RxList<AssistenceEntity> assistences = <AssistenceEntity>[].obs;
  final Rx<TextEditingController> observationController =
      TextEditingController().obs;
  @override
  void onReady() {
    super.onReady();
    this.getAssistences();
  }

  void getAssistences() async {
    if (this.global.studentsGroup.isEmpty) await this.global.getStudents();
    if (assistenceSerData.validateAssistences()) {
      this.global.studentsGroup.forEach((student) => this.assistences.add(
          AssistenceEntity(studentId: student.id, assist: AssistenceEnum.YES)));
    } else
      this.assistences.value = assistenceSerData
          .getAll()
          .map((assistence) => (AssistenceEntity.fromObjectHive(assistence)))
          .toList();
  }

  void checkAssistences() async {
    bool sentDataServer = false;
    await assistenceSerData.deleting();
    LoadingDialog.showLoadingDialog(text: 'Tomando asistencia');
    final resp = await assistenceSerData.addAll(
        this.assistences.map((assistent) => assistent.toObjectHive()).toList());
    if (this.global.connection.value) {
      if (await Connection.connection) {
        final resp = await assistenceService.save(this.assistences);
        if (resp.data.state) {
          assistenceSerData.sent();
          sentDataServer = true;
          Get.back();
          ResponseWidget.snackbar(title: 'Asistencia', msg: 'Se registraron las asistencia de manera correcta', type: TypeDialog.OK);
        } else {
          // Get.back();
          // ResponseWidget.snackbar(title: 'Asistencia', msg: resp.data.message, type: TypeDialog.ERROR);
        }
      } else {
        Get.back();
        ResponseWidget.snackbar(
            title: 'Conexion',
            msg: 'Estas sin conexión a internet',
            type: TypeDialog.ERROR);
      }
    }
    if (!sentDataServer) {
      if (resp.state) {
        Get.back();
        ResponseWidget.snackbar(
            title: 'Asistencia (local)', msg: resp.message, type: TypeDialog.OK);
      } else {
        Get.back();
        ResponseWidget.snackbar(
            title: 'Asistencia (local)',
            msg: resp.message,
            type: TypeDialog.ERROR);
      }
    } 
  }

  void sentData() async {
    int noSent = assistenceSerData.getAllNoSent().length;
    if (noSent > 0) {
      if (this.global.connection.value) {
        if (await Connection.connection) {
          LoadingDialog.showLoadingDialog(text: 'Sincronizando asistencia');
          final resp = await assistenceService.save(this.assistences);
          if (resp.data.state) {
            Get.back();
            assistenceSerData.sentData();
            ResponseWidget.snackbar(
                title: 'Asistencia',
                msg: 'Sincronización exitosa',
                type: TypeDialog.OK);
          } else {
            Get.back();
            ResponseWidget.snackbar(
                title: 'Registro de asistencia',
                msg: resp.data.message,
                type: TypeDialog.ERROR);
          }
        } else {
          ResponseWidget.snackbar(
              title: 'Conexión', msg: 'Sin conexión', type: TypeDialog.ERROR);
        }
      }
    } else {
      ResponseWidget.snackbar(
          title: 'Asistencia',
          msg: 'Los datos ya estan sincronizados',
          type: TypeDialog.OK);
    }
  }
}
