import 'package:get/get.dart';
import 'package:practics_movil/src/data/service/AttentionSerData.dart';

class SentDataController extends GetxController {
  // RxInt attentions = 0.obs;
  int attentions() {
    return attentionSerData.getAllNoSent().length;
  }
}
