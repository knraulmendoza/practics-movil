import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/presentation/pages/SentData/SentDataController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';

class SentDataPage extends GetView<SentDataController> {
  const SentDataPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(SentDataController());
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Sincronizar datos'
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('Atenciones'),
            trailing: Text(controller.attentions().toString()),
          )
        ],
      ),
    );
  }
}
