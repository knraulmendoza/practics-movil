import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/presentation/pages/patient/PatientController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawer.dart';
import 'package:reactive_forms/reactive_forms.dart';

class PatientPage extends GetView<PatientController> {
  @override
  Widget build(BuildContext context) {
    Get.put(PatientController());
    return Scaffold(
      appBar: AppBarWidget(
          title: 'Pacientes'),
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.all(10),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ReactiveForm(
                formGroup: this.controller.form,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Theme(
                      data: ThemeData(primaryColor: Colors.green),
                      child: ReactiveTextField(
                        formControlName: 'document',
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(8.0),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          hintText: 'Documento de identidad',
                          hintStyle:
                              TextStyle(color: Color.fromRGBO(0, 147, 57, 1)),
                        ),
                        validationMessages: (control) => {
                          ValidationMessage.required:
                              'Este campo es obligatorio',
                          ValidationMessage.minLength: 'Campo incorrecto',
                          ValidationMessage.maxLength: 'Campo incorrecto'
                        },
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ReactiveFormConsumer(
                      builder: (context, form, child) {
                        return Container(
                          width: Get.width * 1,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.green,
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                  ),
                                  elevation: 0.0),
                              child: Text('Consultar paciente',
                                  style: TextStyle(
                                    color: Colors.white,
                                  )),
                              onPressed: form.valid
                                  ? () => this.controller.searchPatient()
                                  : null),
                        );
                      },
                    ),
                  ],
                ),
              ),
              Obx(() => this.controller.patient.value.document == null
                  ? Container()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: Get.width * 1,
                          child: Text(
                            'Nombre',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          width: Get.width * 1,
                          child: Text(
                            this.controller.patient.value.name,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Tipo de documento',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold)),
                                Text(
                                    this
                                        .controller
                                        .patient
                                        .value
                                        .documentTypeText,
                                    style: TextStyle(fontSize: 18)),
                                SizedBox(
                                  height: 10,
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Documento',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold)),
                                Text(this.controller.patient.value.document,
                                    style: TextStyle(fontSize: 18)),
                                SizedBox(
                                  height: 10,
                                ),
                              ],
                            )
                          ],
                        ),
                        ReactiveForm(
                          formGroup: this.controller.formUpdate,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Edad',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              Theme(
                                data: ThemeData(primaryColor: Colors.green),
                                child: ReactiveTextField(
                                  formControlName: 'age',
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.all(10),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    hintText: 'Edad',
                                    hintStyle: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                  ),
                                  validationMessages: (control) => {
                                    ValidationMessage.required:
                                        'Este campo es obligatorio',
                                  },
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text('Genero',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(
                                height: 5,
                              ),
                              ReactiveDropdownField<Object>(
                                  formControlName: 'gender',
                                  hint: Text(
                                    'Genero',
                                    style: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                  ),
                                  isExpanded: true,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.all(8.0),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    hintStyle: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                  ),
                                  items: this
                                      .controller
                                      .genders
                                      .map((academicLevel) => DropdownMenuItem(
                                            value: academicLevel['value'],
                                            child: FittedBox(
                                              child:
                                                  Text(academicLevel['text']),
                                              fit: BoxFit.contain,
                                            ),
                                          ))
                                      .toList()),
                              SizedBox(
                                height: 10,
                              ),
                              Text('Nivel academico',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(
                                height: 5,
                              ),
                              ReactiveDropdownField<Object>(
                                  formControlName: 'academicLevel',
                                  hint: Text(
                                    'Nivel academico',
                                    style: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                  ),
                                  isExpanded: true,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.all(8.0),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    hintStyle: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                  ),
                                  items: this
                                      .controller
                                      .academicLevels
                                      .map((academicLevel) => DropdownMenuItem(
                                            value: academicLevel['value'],
                                            child: FittedBox(
                                              child:
                                                  Text(academicLevel['text']),
                                              fit: BoxFit.contain,
                                            ),
                                          ))
                                      .toList()),
                              SizedBox(
                                height: 10,
                              ),
                              Text('Situación laboral',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(
                                height: 5,
                              ),
                              ReactiveDropdownField<Object>(
                                  formControlName: 'employmentSituation',
                                  hint: Text(
                                    'Situación laboral',
                                    style: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                  ),
                                  isExpanded: true,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.all(8.0),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    hintStyle: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                  ),
                                  items: this
                                      .controller
                                      .employmentSituation
                                      .map((academicLevel) => DropdownMenuItem(
                                            value: academicLevel['value'],
                                            child: FittedBox(
                                              child:
                                                  Text(academicLevel['text']),
                                              fit: BoxFit.contain,
                                            ),
                                          ))
                                      .toList()),
                              SizedBox(
                                height: 10,
                              ),
                              Text('Procedencia',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              Theme(
                                data: ThemeData(primaryColor: Colors.green),
                                child: ReactiveTextField(
                                  formControlName: 'origin',
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.all(10),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    hintText: 'Procedencia',
                                    hintStyle: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                  ),
                                  validationMessages: (control) => {
                                    ValidationMessage.required:
                                        'Este campo es obligatorio',
                                  },
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text('Estrato socioecónomico',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(
                                height: 5,
                              ),
                              ReactiveDropdownField<Object>(
                                  formControlName: 'status',
                                  hint: Text(
                                    'Estrato socioecónomico',
                                    style: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                  ),
                                  isExpanded: true,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.all(8.0),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    hintStyle: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                  ),
                                  items: this
                                      .controller
                                      .statusList
                                      .map((academicLevel) => DropdownMenuItem(
                                            value: academicLevel['value'],
                                            child: FittedBox(
                                              child:
                                                  Text(academicLevel['text']),
                                              fit: BoxFit.contain,
                                            ),
                                          ))
                                      .toList()),
                              SizedBox(
                                height: 10,
                              ),
                              Text('Telefono',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              Theme(
                                data: ThemeData(primaryColor: Colors.green),
                                child: ReactiveTextField(
                                  formControlName: 'phone',
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.all(10),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    hintText: 'Telefono',
                                    hintStyle: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                  ),
                                  validationMessages: (control) => {
                                    ValidationMessage.required:
                                        'Este campo es obligatorio',
                                  },
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text('Atenciones',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(
                                height: 5,
                              ),
                              Obx(
                                () => ReactiveDropdownField<int>(
                                    formControlName: 'attention',
                                    hint: Text(
                                      'Atenciones',
                                      style: TextStyle(
                                          color: Color.fromRGBO(0, 147, 57, 1)),
                                    ),
                                    isExpanded: true,
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.all(8.0),
                                      border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      ),
                                      hintStyle: TextStyle(
                                          color: Color.fromRGBO(0, 147, 57, 1)),
                                    ),
                                    items: this
                                        .controller
                                        .patient
                                        .value
                                        .attentions
                                        .map((attention) => DropdownMenuItem(
                                              value: attention.id,
                                              child: FittedBox(
                                                child: Text(
                                                    Jiffy(attention.date)
                                                        .format('yyy-MM-dd')),
                                                fit: BoxFit.contain,
                                              ),
                                            ))
                                        .toList()),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              ReactiveFormConsumer(
                                builder: (context, form, child) {
                                  return Container(
                                    width: Get.width * 1,
                                    child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            primary: Colors.green,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      30.0),
                                            ),
                                            elevation: 0.0),
                                        child: Text('Realizar encuesta',
                                            style: TextStyle(
                                              color: Colors.white,
                                            )),
                                        onPressed: form.valid
                                            ? () =>
                                                this.controller.updatePatient()
                                            : null),
                                  );
                                },
                              ),
                            ],
                          ),
                        )
                      ],
                    )),
            ],
          ),
        ),
      ),
      drawer: MenuDrawer(),
    );
  }
}
