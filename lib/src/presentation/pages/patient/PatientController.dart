import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/attention/AttentionState.dart';
import 'package:practics_movil/src/domain/entities/patient/PatientEntity.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:practics_movil/src/services/AttentionService.dart';
import 'package:practics_movil/src/services/PatientService.dart';
import 'package:reactive_forms/reactive_forms.dart';

class PatientController extends GetxController {
  final form = FormGroup({
    'document': FormControl<String>(
      validators: [
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(11)
      ],
    ),
  });
  final formUpdate = FormGroup({
    'gender': FormControl<int>(
      validators: [Validators.required],
    ),
    'age': FormControl<int>(
      validators: [Validators.required],
    ),
    'origin': FormControl<String>(
      validators: [Validators.required],
    ),
    'status': FormControl<int>(
      validators: [Validators.required],
    ),
    'academicLevel': FormControl<int>(
      validators: [Validators.required],
    ),
    'employmentSituation': FormControl<int>(
      validators: [Validators.required],
    ),
    'phone': FormControl<String>(
      validators: [Validators.required],
    ),
    'attention': FormControl<int>(
      validators: [Validators.required],
    ),
  });
  Rx<PatientEntity> patient = PatientEntity(
    document: null,
    name: null,
  ).obs;
  final academicLevels = [
    {'text': 'Primaria', 'value': 1},
    {'text': 'Secundaria', 'value': 2},
    {'text': 'Técnico', 'value': 3},
    {'text': 'Profesional', 'value': 4},
  ];
  final genders = [
    {'text': 'Masculino', 'value': 1},
    {'text': 'Femenino', 'value': 2},
  ];
  final statusList = [
    {'text': 'uno', 'value': 1},
    {'text': 'dos', 'value': 2},
    {'text': 'tres', 'value': 3},
    {'text': 'cuatro', 'value': 4},
  ];
  final employmentSituation = [
    {'text': 'Desempleado', 'value': 1},
    {'text': 'Empleado', 'value': 2},
    {'text': 'Estudiante', 'value': 3},
  ];
  @override
  void onInit() {
    super.onInit();
  }

  searchPatient() async {
    LoadingDialog.showLoadingDialog(text: 'Consultando paciente ...');
    final resp = await attentionService.search(this.form.value['document']);
    Get.back();
    if (resp.data.state) {
      this.patient.value =
          PatientEntity.fromJson(resp.data.data['patient']);
      this.formUpdate.control('gender').value = this.patient.value.gender;
      this.formUpdate.control('age').value = this.patient.value.age ?? 0;
      this.formUpdate.control('origin').value = this.patient.value.origin;
      this.formUpdate.control('status').value = this.patient.value.status;
      this.formUpdate.control('academicLevel').value =
          this.patient.value.academicLevel;
      this.formUpdate.control('employmentSituation').value =
          this.patient.value.employmentSituation;
      this.formUpdate.control('phone').value = this.patient.value.phone;
    } else {
      ResponseWidget.snackbar(
          title: 'Error',
          msg: 'El paciente consultado no esta registrado',
          type: TypeDialog.ERROR);
    }
  }

  updatePatient() async {
    LoadingDialog.showLoadingDialog(text: 'Actualizando datos ...');
    final resp = await patientService.updatePatient(
        this.patient.value.id, PatientEntity.fromForm(this.formUpdate.value));
    Get.back();
    if (resp.data.state) {
      Get.toNamed('/encuesta', arguments: {
        'name': this.patient.value.name,
        'patient_id': this.patient.value.id,
        'attention_id': this.formUpdate.value['attention']
      });
    }
  }
}
