import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/teacher/TeacherEntity.dart';
import 'package:practics_movil/src/domain/enums/RolEnum.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:practics_movil/src/services/PersonService.dart';
import 'package:reactive_forms/reactive_forms.dart';

class TeachersController extends GetxController {
  final data = Get.arguments;
  Rx<TeacherEntity> teacher = new TeacherEntity(personEntity: null).obs;
  final form = FormGroup({
    'document': FormControl<String>(
      validators: [
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(11)
      ],
    ),
  });

  @override
  void onReady() async {
    super.onReady();
    if (this.data != null) {
      this.form.control('document').value = this.data['document'] ?? '';
      this.searchTeacher();
    }
  }

  searchTeacher() async {
    LoadingDialog.showLoadingDialog(text: 'Consultando profesor ...');
    final resp =
        await personService.teacherByDocument(this.form.value['document']);
    Get.back();
    if (resp.data.state) {
      this.teacher.value = TeacherEntity.fromJson(resp.data.data['teacher']);
    } else {
      ResponseWidget.snackbar(
          title: 'Error',
          msg: 'El Profesor consultado no existe',
          type: TypeDialog.ERROR);
    }
  }

  toCheck() {
    if (this.teacher.value.checks < 3) {
      Get.toNamed('/checks', arguments: {
        'id': this.teacher.value.personEntity.id,
        'name': this.teacher.value.personEntity.fullName,
        'rol': RolEnum.TEACHER_PRACTICT
      });
    } else {
      ResponseWidget.snackbar(
          title: 'Chequeo',
          msg: 'Este Profesor ya excedió el número de chequeos por rotación',
          type: TypeDialog.ERROR);
    }
  }
}
