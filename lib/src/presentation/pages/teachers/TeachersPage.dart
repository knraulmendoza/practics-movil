import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/domain/enums/RolEnum.dart';
import 'package:practics_movil/src/presentation/pages/teachers/TeachersController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:reactive_forms/reactive_forms.dart';

class TeachersPage extends GetView<TeachersController> {
  @override
  Widget build(BuildContext context) {
    Get.put(TeachersController());
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Profesores',
        leading: IconButton(onPressed: () => Get.back(), icon: Icon(Icons.chevron_left, color: Colors.blue,)),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            ReactiveForm(
              formGroup: this.controller.form,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Theme(
                    data: ThemeData(primaryColor: Colors.green),
                    child: ReactiveTextField(
                      formControlName: 'document',
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(8.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        hintText: 'Documento de identidad',
                        hintStyle:
                            TextStyle(color: Color.fromRGBO(0, 147, 57, 1)),
                      ),
                      validationMessages: (control) => {
                        ValidationMessage.required: 'Este campo es obligatorio',
                        ValidationMessage.minLength: 'Campo incorrecto',
                        ValidationMessage.maxLength: 'Campo incorrecto'
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  ReactiveFormConsumer(
                    builder: (context, form, child) {
                      return Container(
                        width: Get.width * 1,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: Colors.green,
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                                elevation: 0.0),
                            child: Text('Consultar Profesor',
                                style: TextStyle(
                                  color: Colors.white,
                                )),
                            onPressed: form.valid
                                ? () => this.controller.searchTeacher()
                                : null),
                      );
                    },
                  ),
                ],
              ),
            ),
            Obx(
              () => this.controller.teacher.value.personEntity == null
                  ? Container()
                  : Expanded(
                      child: ListView(
                        children: [
                          ListTile(
                              title: Text(
                                'Nombre',
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                this
                                    .controller
                                    .teacher
                                    .value
                                    .personEntity
                                    .fullName,
                                style: TextStyle(fontSize: 20),
                              )),
                          ListTile(
                            title: Text(
                              'Teléfono',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            subtitle: Text(
                              this.controller.teacher.value.personEntity.phone,
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                          ListTile(
                            title: Text(
                              'Dirección',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            subtitle: Text(
                              this
                                  .controller
                                  .teacher
                                  .value
                                  .personEntity
                                  .address,
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                          ListTile(
                            title: Text(
                              'Genero',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            subtitle: Text(
                              this
                                  .controller
                                  .teacher
                                  .value
                                  .personEntity
                                  .genderText,
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Divider(
                            color: Colors.grey,
                          ),
                          ListTile(
                            title: Text(
                              'Chequeos por rotación',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            subtitle: Text(
                                this
                                    .controller
                                    .teacher
                                    .value
                                    .checksByRotations
                                    .toString(),
                                style: TextStyle(fontSize: 20)),
                          ),
                          ListTile(
                            title: Text(
                              'Chequeos en el semestre',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            subtitle: Text(
                                this.controller.teacher.value.checks.toString(),
                                style: TextStyle(fontSize: 20)),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Row(
        children: <Widget>[
          Expanded(
              child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                  ),
                  child: Text('Nuevo Chequeo'),
                  onPressed: (){
                    this.controller
                      .teacher
                      .value
                      .checksByRotations > 0 ? chequeo(checks: this.controller
                      .teacher
                      .value
                      .checksByRotations):
                      Get.toNamed('/checks', arguments: {
                        'id': this.controller.teacher.value.personEntity.id,
                        'name': this.controller.teacher.value.personEntity.fullName,
                        'rol': RolEnum.TEACHER_PRACTICT
                      });
                  },
            ),
          )
          ),
        ],
      ),
    );
  }
  void chequeo({@required int checks}) {
    Get.defaultDialog(
        radius: 20,
        title: 'Chequeo',
        content: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
                child: Text('Consultar'),
                onPressed: () {
                        Get.toNamed('/comparatorCheck', arguments: {
                          'evaluated': this.controller.teacher.value.personEntity.id,
                          'name': this.controller.teacher.value.personEntity.fullName
                        });
                      }),
            SizedBox(
              width: 10,
            ),
            ElevatedButton(
                child: Text('Registrar'),
                onPressed: () => this.controller.toCheck())
          ],
        )
    );
  }
}
