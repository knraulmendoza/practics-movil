import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/securityQuestion/SecurityQuestion.dart';
import 'package:practics_movil/src/presentation/pages/login/LoginController.dart';
import 'package:reactive_forms/reactive_forms.dart';

class LoginPage extends GetView<LoginController> {
  // final LoginController loginController = Get.put(LoginController());
  @override
  Widget build(BuildContext context) {
    Get.put(LoginController());
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Stack(children: [
            Container(
              height: Get.height * 1,
              // color: Colors.white,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Color.fromRGBO(13, 140, 220, 1), Colors.white],
                      stops: [0.4, 0.8],
                      begin: FractionalOffset.topCenter,
                      end: FractionalOffset.bottomCenter),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0),
                  )),
            ),
            Container(
              width: Get.width * 1,
              child: Image.asset(
                'assets/images/logo_blanco.png',
                fit: BoxFit.contain,
              ),
            ),
            Container(
              child: ReactiveForm(
                  formGroup: this.controller.form,
                  child: Stack(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: Get.height * 0.33),
                        padding: EdgeInsets.all(10),
                        child: Card(
                          elevation: 10.0,
                          shape: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.transparent),
                              borderRadius: BorderRadius.circular(20.0)),
                          child: Container(
                            color: Colors.blueAccent[300],
                            padding: EdgeInsets.all(20),
                            child: SingleChildScrollView(
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'Iniciar sesión',
                                    style: TextStyle(
                                      fontSize: 30.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Theme(
                                    data: ThemeData(primaryColor: Colors.green),
                                    child: ReactiveTextField(
                                      formControlName: 'username',
                                      decoration: InputDecoration(
                                        // border: InputBorder.none,
                                        contentPadding: const EdgeInsets.all(8.0),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(30.0),
                                            borderSide:
                                                BorderSide(color: Colors.green)),
                                        hintText: 'Usuario',
                                        hintStyle: TextStyle(
                                            color: Color.fromRGBO(0, 147, 57, 1)),
                                        prefixIcon: Icon(Icons.person),
                                      ),
                                      validationMessages: (control) => {
                                        ValidationMessage.required:
                                            'El usuario es obligatorio',
                                      },
                                      textInputAction: TextInputAction.next,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Obx(() => Theme(
                                        data:
                                            ThemeData(primaryColor: Colors.green),
                                        child: ReactiveTextField(
                                          formControlName: 'password',
                                          obscureText: !this
                                              .controller
                                              .iconVisibility
                                              .value,
                                          decoration: InputDecoration(
                                              contentPadding:
                                                  const EdgeInsets.all(8.0),
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(30.0),
                                              ),
                                              hintText: 'Contraseña',
                                              hintStyle: TextStyle(
                                                  color: Color.fromRGBO(
                                                      0, 147, 57, 1)),
                                              prefixIcon: Icon(Icons.lock),
                                              suffixIcon: Obx(() => IconButton(
                                                  icon: Icon(this
                                                          .controller
                                                          .iconVisibility
                                                          .value
                                                      ? Icons.visibility
                                                      : Icons.visibility_off),
                                                  onPressed: () {
                                                    this
                                                            .controller
                                                            .iconVisibility
                                                            .value =
                                                        !this
                                                            .controller
                                                            .iconVisibility
                                                            .value;
                                                  }))),
                                          validationMessages: (control) => {
                                            ValidationMessage.required:
                                                'El usuario es obligatorio',
                                          },
                                          textInputAction: TextInputAction.next,
                                        ),
                                      )),
                                  SizedBox(height: 20),
                                  GestureDetector(
                                    child: Text('¿Olvido su contraseña?'),
                                    onTap: () => {this.dialogPassword()},
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  ReactiveFormConsumer(
                                    builder: (context, form, child) {
                                      return Container(
                                        width: Get.width * 1,
                                        child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                              primary: Colors.green,
                                              elevation: 0.0,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(30))),
                                          child: Text('Iniciar sesión',
                                              style: TextStyle(
                                                color: Colors.white,
                                              )),
                                          onPressed: form.valid
                                              ? () =>
                                                  this.controller.authenticate()
                                              : null,
                                        ),
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: Get.width * 0.8,
                margin: EdgeInsets.only(bottom: 10),
                child: Image.asset('assets/images/logo_upc.png'),
              ),
            )
          ]),
        ),
      ),
    );
  }

  void dialogPassword() {
    Get.defaultDialog(
        title: 'Olvido su contraseña',
        radius: 20,
        content: Container(
            child: Obx(() => this.controller.userId.value == 0
                ? ReactiveForm(
                    formGroup: this.controller.formVerifyUser,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Theme(
                          data: ThemeData(primaryColor: Colors.green),
                          child: ReactiveTextField(
                            formControlName: 'username',
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(8.0),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              hintText: 'Usuario',
                              hintStyle: TextStyle(
                                  color: Color.fromRGBO(0, 147, 57, 1)),
                              prefixIcon: Icon(Icons.person),
                            ),
                            validationMessages: (control) => {
                              ValidationMessage.required:
                                  'Este campo es obligatorio',
                            },
                            textInputAction: TextInputAction.next,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Theme(
                          data: ThemeData(primaryColor: Colors.green),
                          child: ReactiveTextField(
                            formControlName: 'document',
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(8.0),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              hintText: 'Documento de identidad',
                              hintStyle: TextStyle(
                                  color: Color.fromRGBO(0, 147, 57, 1)),
                              prefixIcon: Icon(Icons.credit_card),
                            ),
                            validationMessages: (control) => {
                              ValidationMessage.required:
                                  'Este campo es obligatorio',
                              ValidationMessage.number: 'Solo números'
                            },
                            textInputAction: TextInputAction.next,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Divider(
                          color: Colors.black,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Obx(
                          () => Container(
                            margin: EdgeInsets.symmetric(horizontal: 5),
                            child: ReactiveDropdownField<int>(
                                formControlName: 'question',
                                hint: Text(
                                  'Preguntas de seguridad',
                                  style: TextStyle(
                                      color: Color.fromRGBO(0, 147, 57, 1)),
                                ),
                                isExpanded: true,
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(8.0),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                  hintStyle: TextStyle(
                                      color: Color.fromRGBO(0, 147, 57, 1)),
                                ),
                                items: this
                                    .controller
                                    .questions
                                    .map((question) => DropdownMenuItem(
                                          value: question.id,
                                          child: FittedBox(
                                            child: Text(question.question),
                                            fit: BoxFit.contain,
                                          ),
                                        ))
                                    .toList()),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Theme(
                          data: ThemeData(primaryColor: Colors.green),
                          child: ReactiveTextField(
                            formControlName: 'answer',
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(8.0),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              hintText: 'Respuesta',
                              hintStyle: TextStyle(
                                  color: Color.fromRGBO(0, 147, 57, 1)),
                            ),
                            validationMessages: (control) => {
                              ValidationMessage.required:
                                  'Este campo es obligatorio',
                            },
                          ),
                        ),
                        ReactiveFormConsumer(
                          builder: (context, form, child) {
                            return Container(
                              width: Get.width * 1,
                              child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      elevation: 0.0,
                                      primary: Colors.green,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(30),
                                        side: BorderSide(
                                            width: 0, color: Colors.white),
                                      )),
                                  child: Text('Validar usuario',
                                      style: TextStyle(
                                        color: Colors.white,
                                      )),
                                  onPressed: form.valid
                                      ? () => this.controller.verifyUser()
                                      : null),
                            );
                          },
                        ),
                      ],
                    ),
                  )
                : ReactiveForm(
                    formGroup: this.controller.formNewPassword,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text('Ingrese una nueva contraseña'),
                        SizedBox(
                          height: 20,
                        ),
                        Obx(() => Theme(
                              data: ThemeData(primaryColor: Colors.green),
                              child: ReactiveTextField(
                                formControlName: 'newPassword',
                                obscureText:
                                    !this.controller.iconVisibility.value,
                                validationMessages: (control) => {
                                  ValidationMessage.required:
                                      'El usuario es obligatorio',
                                },
                                decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.all(8.0),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    hintText: 'Nueva Contraseña',
                                    hintStyle: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                    prefixIcon: Icon(Icons.lock),
                                    suffixIcon: Obx(() => IconButton(
                                        icon: Icon(
                                            this.controller.iconVisibility.value
                                                ? Icons.visibility
                                                : Icons.visibility_off),
                                        onPressed: () {
                                          this.controller.iconVisibility.value =
                                              !this
                                                  .controller
                                                  .iconVisibility
                                                  .value;
                                        }))),
                                textInputAction: TextInputAction.next,
                              ),
                            )),
                        SizedBox(
                          height: 10,
                        ),
                        Obx(() => Theme(
                              data: ThemeData(primaryColor: Colors.green),
                              child: ReactiveTextField(
                                formControlName: 'repeatNewPassword',
                                obscureText:
                                    !this.controller.iconVisibility.value,
                                decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.all(8.0),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    hintText: 'Repetir Nueva Contraseña',
                                    hintStyle: TextStyle(
                                        color: Color.fromRGBO(0, 147, 57, 1)),
                                    prefixIcon: Icon(Icons.lock),
                                    suffixIcon: Obx(() => IconButton(
                                        icon: Icon(
                                            this.controller.iconVisibility.value
                                                ? Icons.visibility
                                                : Icons.visibility_off),
                                        onPressed: () {
                                          this.controller.iconVisibility.value =
                                              !this
                                                  .controller
                                                  .iconVisibility
                                                  .value;
                                        }))),
                                validationMessages: (control) => {
                                  ValidationMessage.required:
                                      'Este campo es obligatorio',
                                },
                                textInputAction: TextInputAction.next,
                              ),
                            )),
                        SizedBox(
                          height: 10,
                        ),
                        ReactiveFormConsumer(
                          builder: (context, form, child) {
                            return Container(
                              width: Get.width * 1,
                              child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      primary: Colors.green,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(30.0),
                                      ),
                                      elevation: 0.0),
                                  child: Text('Cambiar contraseña',
                                      style: TextStyle(
                                        color: Colors.white,
                                      )),
                                  onPressed: form.valid
                                      ? () =>
                                          this.controller.changeNewPassword()
                                      : null),
                            );
                          },
                        ),
                      ],
                    ),
                  ))));
  }
}
