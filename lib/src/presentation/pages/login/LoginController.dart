import 'dart:convert';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:practics_movil/src/domain/entities/groupModule/GroupEntity.dart';
import 'package:practics_movil/src/domain/entities/rol/RolEntity.dart';
import 'package:practics_movil/src/domain/entities/securityQuestion/SecurityQuestion.dart';
import 'package:practics_movil/src/domain/entities/user/UserEntity.dart';
import 'package:practics_movil/src/domain/enums/RolEnum.dart';
import 'package:practics_movil/src/helpers/Connection.dart';
import 'package:practics_movil/src/services/GroupStudentService.dart';
import 'package:practics_movil/src/services/LoginService.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:reactive_forms/reactive_forms.dart';
class LoginController extends GetxController {
  RxBool iconVisibility = false.obs;
  RxString storage = ''.obs;
  RxInt userId = 0.obs;
  RxList<SecurityQuestionEntity> questions = <SecurityQuestionEntity>[].obs;
  final form = FormGroup({
    'username': FormControl(
      validators: [
        Validators.required,
      ],
    ),
    'password': FormControl(validators: [
      Validators.required,
    ])
  });
  final formVerifyUser = FormGroup({
    'username': FormControl<String>(
      validators: [Validators.required],
    ),
    'document': FormControl<String>(validators: [
      Validators.required,
      Validators.number,
    ]),
    'question': FormControl<int>(validators: [
      Validators.required,
    ]),
    'answer': FormControl<String>(
      validators: [Validators.required],
    ),
  });
  final formNewPassword = FormGroup({
    'newPassword': FormControl(
      validators: [
        Validators.required,
      ],
    ),
    'repeatNewPassword': FormControl(validators: [
      Validators.required,
    ])
  });
  void changeIconVisibility() {
    this.iconVisibility.value = !this.iconVisibility.value;
  }

  @override
  void onInit() {
    super.onInit();
    this.storage.value = GetStorage().read('test');
    this.showQuestions();
  }

  void showQuestions() async {
    final resp = await loginService.securityQuestions();
    if (resp.data.state) {
      this.questions.value = (resp.data.data['security_questions'] as List)
          .map((question) => new SecurityQuestionEntity.fromJson(question))
          .toList();
    }
  }

  void authenticate() async {
    final storage = GetStorage();
    if (await Connection.connection) {
      LoadingDialog.showLoadingDialog(text: 'Iniciando sesion ...');
      final resp =
          await loginService.authenticate(UserEntity.fromForm(this.form.value));
      if (resp.data.state) {
        final responseLogin = resp.data.data;
        RolEntity rol;
        int career;
        String careerName;
        rol = RolEntity.fromJson(responseLogin['user']['role']);
        // if ((responseLogin['user']['roles'] as List).length > 1) {
        //   final rolAux = (responseLogin['user']['roles'] as List)
        //       .map((rol) => new RolEntity.fromJson(rol))
        //       .toList()
        //       .firstWhere((rol) => rol.id == RolEnum.TEACHER_PRACTICT,
        //           orElse: () => null);
        //   rol = rolAux;
        //   storage.write(
        //       'roles', jsonEncode(responseLogin['user']['roles'] as List));
        // }
        if ((responseLogin['careers'] as List).length > 1) {
          storage.write(
              'careers', jsonEncode(responseLogin['user']['careers'] as List));
        } else {
          career = responseLogin['careers'][0]['id'];
          careerName = responseLogin['careers'][0]['name'];
        }
        Map currentUser = {
          'id': responseLogin['user']['id'],
          'user': responseLogin['user']['user'],
          'rol': rol.toJson(),
          'career': career,
          'careerName': careerName,
          'state_password': responseLogin['user']['state_password']
        };
        storage.write('currentUser', jsonEncode(currentUser));
        storage.write('token', responseLogin['token']);
        storage.write('authenticated', true);
        if (rol.id == RolEnum.STUDENT) getGroup();
        Get.back();
        responseLogin['user']['state_password'] == '0'
            ? Get.offAllNamed('/change-password')
            : Get.offAllNamed('/');
      } else {
        Get.back();
        ResponseWidget.snackbar(
            title: 'Error',
            msg:  resp.data.message,
            type: TypeDialog.ERROR);
        loginService.deleteGetLocal();
      }
    } else {
      ResponseWidget.snackbar(
          title: 'Error', msg: 'Sin conexión', type: TypeDialog.ERROR);
    }
  }

  getGroup() async {
    final respGroup = await groupStudentService.getGroup();
    if (respGroup.data.state) {
      final group = GroupEntity.fromJson(respGroup.data.data['group']);
      GetStorage().write('group', jsonEncode(group.toJsonSQlite()));
    } else {
      ResponseWidget.snackbar(
          title: 'Error grupo',
          msg: respGroup.data.message,
          type: TypeDialog.ERROR);
      loginService.deleteGetLocal();
    }
  }

  void verifyUser() async {
    LoadingDialog.showLoadingDialog(text: 'Verificando información ...');
    final resp = await loginService.verifyUser(
        this.formVerifyUser.value['username'],
        this.formVerifyUser.value['document'],
        this.formVerifyUser.value['question'],
        this.formVerifyUser.value['answer']);
    Get.back();
    if (resp.data.state) {
      ResponseWidget.snackbar(
          title: 'Verificando información',
          msg: 'Datos correcta',
          type: TypeDialog.OK);
      this.userId.value = resp.data.data['user']['id'];
    } else {
      ResponseWidget.snackbar(
          title: 'Olvido su contraseña',
          msg: resp.data.message,
          type: TypeDialog.ERROR);
    }
  }

  changeNewPassword() async {
    if (this.formNewPassword.value["newPassword"] ==
        this.formNewPassword.value["repeatNewPassword"]) {
      LoadingDialog.showLoadingDialog(text: 'Cambiando contraseña');
      final resp = await loginService.updatedPassword(
          this.formNewPassword.value["newPassword"],
          this.formVerifyUser.value['answer'],
          userId: this.userId.value,
          securityQuestionId: this.formVerifyUser.value['question']);
      if (resp.data.state) {
        this.form.control('username').value =
            this.formVerifyUser.value['username'];
        this.form.control('password').value =
            this.formNewPassword.value["newPassword"];
        Get.back();
        this.authenticate();
      } else {
        Get.back();
      }
    } else {
      ResponseWidget.snackbar(
          title: 'Error',
          msg: 'Las contraseñas no coinciden',
          type: TypeDialog.ERROR);
    }
  }
}
