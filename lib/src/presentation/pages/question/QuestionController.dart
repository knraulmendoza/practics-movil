import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/student/StudentEntity.dart';
import 'package:practics_movil/src/helpers/Connection.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:practics_movil/src/services/GroupStudentService.dart';

class QuestionController extends GetxController {
  RxList<StudentEntity> students = <StudentEntity>[].obs;
  @override
  void onReady() {
    super.onReady();
  }

  getStudents() async {
    if (await Connection.connection) {
      LoadingDialog.showLoadingDialog(text: 'Cargando estudiantes');
      final resp = await groupStudentService.getStudentGroupByTeacher();
      if (resp.data.state) {
        this.students.value = (resp.data.data['studentsGroup'] as List)
            .map((student) => StudentEntity.fromJson(student))
            .toList();
      }
      Get.back();
    } else {
      ResponseWidget.snackbar(
          title: 'Conexión',
          msg: 'No tienes conexión a internet',
          type: TypeDialog.ERROR);
    }
  }
}
