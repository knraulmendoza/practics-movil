import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/pages/question/QuestionController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';

class QuestionPage extends GetView<QuestionController> {
  @override
  Widget build(BuildContext context) {
    Get.put(QuestionController());
    final global = Get.put(GlobalsController());
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Estudiantes a hacer chequeos',
      ),
      body: ListView.builder(
          itemCount: global.studentsGroup.length,
          itemBuilder: (_, i) {
            return ListTile(
              title: Text(global.studentsGroup[i].personEntity.fullName),
            );
          }),
    );
  }
}
