import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/answer/AnswerEntity.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/pages/checksComparator/ChecksComparatorController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawer.dart';
import 'package:collection/collection.dart';

class ChecksComparatorPage extends GetView<ChecksComparatorController> {
  const ChecksComparatorPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(ChecksComparatorController());
    final global = Get.find<GlobalsController>();
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0), // here the desired height
          child: AppBarWidget(
            title: 'Comparando chequeos',
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(40.0),
              child: Text(
                  this.controller.data != null
                      ? this.controller.data['name']
                      : global.person.value.fullName,
                  style: TextStyle(fontSize: 18),
                ),
            ),
            icons: [IconButton(
                icon: Icon(Icons.refresh, color: Colors.blue,),
                onPressed: () => controller.getQuestionsWithAnswers())],
          ),),
      body: Obx(() => 
              controller
              .questions
              .every((question) => question.answers.length < 1)
          ? Center(
              child: Container(
                padding: EdgeInsets.all(20),
                child: Text(
                  'Todavía no tienes chequeos en esta rotación',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20),
                ),
              ),
            )
          : ListView.builder(
              padding: EdgeInsets.only(top: 10),
              itemCount: controller.questions.value.length,
              itemBuilder: (_, i) {
                return ListTile(
                  title: Text(
                    '${i + 1}. ' +
                        this.controller.questions[i].question.toLowerCase(),
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: List.generate(
                          this.controller.questions[i].answers.length, (index) {
                        return ListTile(
                          dense: true,
                          title: Text('Chequeo ${index + 1}'),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Checkbox(
                                value: controller.questions[i].answers[index]
                                            .answer ==
                                        1
                                    ? true
                                    : false,
                                onChanged:
                                    null, //(value)=>controller.answers[i].answer = value?1:0,
                                activeColor: Colors.blue[400],
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              IconButton(
                                  icon: Icon(Icons.edit),
                                  onPressed: () => observationAnswers(controller
                                          .questions[i].answers[
                                      index])) //()=>observationAnswers(controller.answers[i]))
                            ],
                          ),
                        );
                      })),
                );
              })),
      // drawer: Get.arguments == null ? MenuDrawer(): null,
      drawer: MenuDrawer(),
      floatingActionButton: Obx(() => !controller.checksChequeado.value ? Container() : FloatingActionButton(
          child: Icon(Icons.check),
          onPressed: () => {chequeo()})),
    );
  }

    void chequeo() {
    Get.defaultDialog(
        radius: 20,
        title: 'Chequeo',
        content: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Obx(() =>
            Column(
                children: controller.checks.value.mapIndexed((index, check) => ListTile(
                  title: Text('Chequeo ${index + 1} ${check['date']}'),
                  trailing: Checkbox(
                    onChanged: check['state'] == 4 ? null : (val) => {
                      if (val) {
                        controller.activeCheck(check['id'], 4)
                      }
                    },
                    value: check['state'] == 4,
                  ))).toList(),
              ),
            )
          ],
        ));
  }

  void observationAnswers(AnswerEntity answerEntity) {
    Get.defaultDialog(
      radius: 20,
      title: 'Observación',
      content: Column(
        children: <Widget>[
          Container(
            height: Get.height * 0.2,
            width: Get.width * 1,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: Colors.grey, width: 1)),
            child: Obx(() => Text(answerEntity.observation ?? '')),
          ),
        ],
      ),
    );
  }
}
