import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/question/QuestionEntity.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:practics_movil/src/services/CheckService.dart';

class ChecksComparatorController extends GetxController {
  final global = Get.find<GlobalsController>();
  final data = Get.arguments;
  RxList<QuestionEntity> questions = <QuestionEntity>[].obs;
  final Rx<TextEditingController> observationController =
      TextEditingController().obs;
  RxList checks = [].obs;
  RxBool checksChequeado = false.obs;

  @override
  void onReady()  async {
    super.onReady();
    await getQuestionsWithAnswers();
  }

  getQuestionsWithAnswers() async {
    LoadingDialog.showLoadingDialog(text: 'Cargando chequeos');
    int evaluated = this.global.person.value.id;
    if (this.data != null) {
      evaluated = this.data['evaluated'];
    }
    final resp = await checkService.checks(evaluated: evaluated);
    if (resp.data.state) {
      this.questions.value = (resp.data.data['questions'] as List)
          .map((question) => QuestionEntity.fromJsonWithAnswers(question))
          .toList();
      this.checks.value = resp.data.data['checks'];
      this.checksChequeado.value = this.checks.value.where((check) => int.parse(check['state']) == 2).toList().isNotEmpty;
    }
    Get.back();
  }

  activeCheck(int id, int state) async {
    LoadingDialog.showLoadingDialog(text: 'Cargando');
    final resp = await checkService.activeChecks(id: id, state: state);
    Get.back();
    Get.back();
    if (resp.data.state) {
      final index = this.checks.value.indexWhere((check) => check['id'] == id);
      this.checks.value[index]['state'] = state.toString();
      this.checksChequeado.value = this.checks.value.where((check) => int.parse(check['state']) == 2).toList().isNotEmpty;
      update();
      ResponseWidget.snackbar(title: 'Check', msg: 'Chequeo actualizado', type: TypeDialog.OK);
    } else {
      ResponseWidget.snackbar(title: 'Check', msg: resp.data.message, type: TypeDialog.ERROR);
    }
  }
}
