import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/pages/AttentionList/AttentionListController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:practics_movil/src/presentation/widgets/listAttention/ListAttentionWidget.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawer.dart';

class AttentionListPage extends GetView<AttentionListController> {
  @override
  Widget build(BuildContext context) {
    Get.put(AttentionListController());
    final global = Get.find<GlobalsController>();
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Atenciones a verificar',
        icons: [
          IconButton(
              icon: Icon(Icons.refresh, color: Colors.blue,),
              onPressed: () => controller.getAttentions()),
              IconButton(
                icon: Icon(Icons.calendar_today, color: Colors.blue,),
                onPressed: () async {
                  final newDateTime = await showRoundedDatePicker(
                    context: context,
                    initialDate: Jiffy(this.controller.date.value).dateTime,
                    firstDate: DateTime(2020, 7),
                    lastDate: Jiffy(this.controller.initialDate.value).dateTime,
                    borderRadius: 16,
                    barrierDismissible: false,
                    height: Get.height * 0.4,
                    // description: 'Si quiere saber que atenciones realizó cierto día por favor revise el calendario y seleccione una fecha en especifica.'
                  );
                  if (!newDateTime.isNull) {
                    this.controller.date.value =
                        Jiffy(newDateTime).format('yyyy-MM-dd');
                    this.controller.getAttentions();
                  }
                })
        ],
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text('Estudiantes'),
            subtitle: Obx(() => Column(
              children: [
                DropdownButton(
                    value: controller.studentId.value,
                    items: global.studentsGroup
                        .map((student) => DropdownMenuItem(
                            child: Text(student.personEntity.fullName),
                            value: student.id))
                        .toList(),
                    onChanged: (int val) {
                      controller.studentId.value = val;
                      controller.getAttentions();
                    }),
                    Text(
                        this.controller.date.value,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
              ],
            )),
          ),
          ListAttentionWidget()
        ],
      ),
      drawer: MenuDrawer(),
    );
  }
}
