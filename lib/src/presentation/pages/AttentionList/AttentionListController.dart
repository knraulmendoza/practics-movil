import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/domain/entities/attention/AttentionEntity.dart';
import 'package:practics_movil/src/domain/entities/student/StudentEntity.dart';
import 'package:practics_movil/src/helpers/Connection.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:practics_movil/src/services/AttentionService.dart';
import 'package:practics_movil/src/services/GroupStudentService.dart';

class AttentionListController extends GetxController {
  RxList<AttentionEntity> attentions = <AttentionEntity>[].obs;
  RxInt studentId = 0.obs;
  RxList<StudentEntity> students = <StudentEntity>[].obs;
  final global = Get.find<GlobalsController>();
  RxString date = ''.obs;
  RxString initialDate = ''.obs;
  @override
  void onReady() async {
    super.onReady();
    this.date.value = Jiffy().format('yyyy-MM-dd');
    this.initialDate.value =
        Jiffy().format('yyyy-MM-dd');
    await this.global.getStudents();
    if (this.global.studentsGroup.isNotEmpty) this.studentId.value = this.global.studentsGroup.first.id;
    getAttentions();
  }

  getStudentsByRotation() async {
    if (await Connection.connection) {
      LoadingDialog.showLoadingDialog(text: 'Cargando estudiantes');
      final resp = await groupStudentService.getStudentGroupByTeacher();
      print(resp.data.state);
      if (resp.data.state) {
        this.students.value = (resp.data.data['studentsGroup'] as List)
            .map((student) => StudentEntity.fromJson(student))
            .toList();
        this.studentId.value = this.students.first.id;
      }
      // await getAttentions();
      Get.back();
    } else {
      ResponseWidget.snackbar(
          title: 'Conexión',
          msg: 'No tienes conexión a internet',
          type: TypeDialog.ERROR);
    }
  }

  getAttentions() async {
    if (await Connection.connection) {
      LoadingDialog.showLoadingDialog(text: 'Cargando atenciones');
      this.global.attentions.value = [];
      final resp =
          await attentionService.getAttentionByStudent(this.studentId.value, this.date.value);
      Get.back();
      if (resp.data.state) {
        this.global.attentions.value = (resp.data.data['attentions'] as List)
            .map((e) => AttentionEntity.fromJson(e))
            .toList();
      }
    } else {
      ResponseWidget.snackbar(
          title: 'Conexión',
          msg: 'No tienes conexión a internet',
          type: TypeDialog.ERROR);
    }
  }
}
