import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/presentation/pages/event/EventController.dart';
import 'package:practics_movil/src/presentation/pages/home/HomeController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';

class EventPage extends GetView<EventController> {
  const EventPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(EventController());
    final eventController = Get.find<HomeController>();
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Evento',
        leading: IconButton(onPressed: () => Get.back(), icon: Icon(Icons.chevron_left, color: Colors.blue,)),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Text(
                controller.event.title,
                style: TextStyle(fontSize: 25),
              ),
            ),
            Image.network(
              controller.event.image,
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.all(10),
              child: Text(
                  '${Jiffy(controller.event.date).format('yyyy-MM-dd HH:mm')}'),
            ),
            HtmlWidget(controller.event.description)
          ],
        ),
      ),
    );
  }
}
