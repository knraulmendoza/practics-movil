import 'package:get/get.dart';
import 'package:practics_movil/src/presentation/controllers/MenuGlobalController.dart';

class MenuPageController extends GetxController {
  @override
  void onReady() {
    super.onReady();
    final menuGlobalController = Get.find<MenuGlobalController>();
    menuGlobalController.getCurrentPage();
    Get.offNamed(menuGlobalController.currentPage.value);
  }
}
