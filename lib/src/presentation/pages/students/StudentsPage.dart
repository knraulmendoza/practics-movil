import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/student/StudentEntity.dart';
import 'package:practics_movil/src/domain/enums/RolEnum.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/pages/students/StudentsController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawer.dart';

class StudentsPage extends GetView<StudentsController> {
  final global = Get.put(GlobalsController());
  @override
  Widget build(BuildContext context) {
    Get.put(StudentsController());
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Estudiantes a hacer chequeos'
      ),
      body: Obx(() => ListView.builder(
          itemCount: global.studentsGroup.length,
          itemBuilder: (_, i) {
            return ListTile(
                title: Text(global.studentsGroup[i].personEntity.fullName),
                trailing:
                    Obx(() => Text(global.studentsGroup[i].checks.toString())),
                onTap: () {
                  controller.student.value = global.studentsGroup[i];
                  global.studentsGroup[i].checks > 0
                      ? chequeo(
                          checks: global.studentsGroup[i].checks,
                          student: global.studentsGroup[i],
                        )
                      : Get.toNamed('/checks', arguments: {
                          'id': global.studentsGroup[i].personEntity.id,
                          'name': global.studentsGroup[i].personEntity.fullName,
                          'rol': RolEnum.STUDENT
                        });
                });
          })),
      drawer: MenuDrawer(),
    );
  }

  void chequeo({@required int checks, StudentEntity student}) {
    Get.defaultDialog(
        radius: 20,
        title: 'Chequeo',
        content: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              children: [1,2].map((i) => ListTile(
                title: Text('Chequeo $i'),
                trailing: Checkbox(
                  onChanged: i==1? null : (val) => {
                    print('yes')
                  },
                  value: i == 4,
                ),
                onTap: student == null
                    ? null
                    : () {
                        Get.toNamed('/comparatorCheck', arguments: {
                          'evaluated': student.personEntity.id,
                          'name': student.personEntity.fullName
                        });
                      })).toList(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                child: Text('Consultar'),
                onPressed: student == null
                    ? null
                    : () {
                        Get.toNamed('/comparatorCheck', arguments: {
                          'evaluated': student.personEntity.id,
                          'name': student.personEntity.fullName
                        });
                      }),
            SizedBox(
              width: 10,
            ),
            ElevatedButton(
                child: Text('Registrar'),
                onPressed: () => this.controller.toCheck(student, checks))
              ],
            )
          ],
        ));
  }
}
