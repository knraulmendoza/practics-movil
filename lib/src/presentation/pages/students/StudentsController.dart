import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/person/PersonEntity.dart';
import 'package:practics_movil/src/domain/entities/student/StudentEntity.dart';
import 'package:practics_movil/src/domain/enums/RolEnum.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';

class StudentsController extends GetxController {
  final global = Get.find<GlobalsController>();
  Rx<StudentEntity> student = new StudentEntity(
          personEntity: new PersonEntity(
              documentType: null,
              document: null,
              firstName: null,
              secondName: null,
              firstLastName: null,
              secondLastName: null,
              phone: null,
              address: null,
              birthDay: null,
              gender: null))
      .obs;
  @override
  void onReady() async {
    super.onReady();
    await this.global.getStudents();
  }

  toCheck(StudentEntity student, int checks) {
    if (checks < 3) {
      Get.toNamed('/checks', arguments: {
        'id': student.personEntity.id,
        'name': student.personEntity.fullName,
        'rol': RolEnum.STUDENT
      });
    } else {
      ResponseWidget.snackbar(
          title: 'Chequeo',
          msg: 'Este estudiante ya excedió el número de chequeos por rotación',
          type: TypeDialog.ERROR);
    }
  }
}
