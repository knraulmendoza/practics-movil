import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/presentation/pages/rotationTeacher/RotationTeacherController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawer.dart';

class RotationTeacherPage extends GetView<RotationTeacherController> {
  @override
  Widget build(BuildContext context) {
    Get.put(RotationTeacherController());
    return Scaffold(
      appBar: AppBarWidget(title: 'Rotaciones'),
      body: Obx(() => ListView.builder(
          itemCount: controller.rotations.length,
          itemBuilder: (_, i) {
            return Column(
              children: <Widget>[
                ListTile(
                  title: Text(
                    'rotación ${i + 1}',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Column(
                  children: List.generate(
                      controller.rotations[i].group.students.length, (j) {
                    return ListTile(
                      visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                      title: Text(controller.rotations[i].group.students[j]
                          .personEntity.fullName),
                      subtitle: Text('Documento: ${controller.rotations[i].group.students[j].personEntity.document}'),
                    );
                  }),
                )
              ],
            );
          })),
      drawer: MenuDrawer(),
    );
  }
}
