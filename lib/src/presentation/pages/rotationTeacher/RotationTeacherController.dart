import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/rotation/RotationEntity.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/services/RotationService.dart';

class RotationTeacherController extends GetxController {
  RxList<RotationEntity> rotations = <RotationEntity>[].obs;
  @override
  void onReady() {
    super.onReady();
    getRotations();
  }

  getRotations() async {
    LoadingDialog.showLoadingDialog(text: 'Cargando rotaciones ...');
    final resp = await rotationService.getrotationsWithGroups();
    if (resp.data.state) {
      this.rotations.value = (resp.data.data['rotations'] as List)
          .map((rotation) => RotationEntity.fromJsonWithGroup(rotation))
          .toList();
    }
    Get.back();
  }
}
