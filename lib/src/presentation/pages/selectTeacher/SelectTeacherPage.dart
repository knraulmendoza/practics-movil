import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/presentation/pages/selectTeacher/SelectTeacherController.dart';
import 'package:practics_movil/src/presentation/widgets/appBarWidget.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawer.dart';

class SelectTeacherPage extends GetView<SelectTeacherController> {
  @override
  Widget build(BuildContext context) {
    Get.put(new SelectTeacherController());
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Consultar docente'
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              height: 200,
              width: 200,
              child: ElevatedButton(
                  onPressed: () {
                    Get.toNamed('/teacher-by-document');
                  },
                  child: Text(
                    'Profesor por documento',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20),
                  )),
            ),
            Container(
              height: 200,
              width: 200,
              child: ElevatedButton(
                  onPressed: () {
                    Get.toNamed('/teacher-by-scenario');
                  },
                  child: Text(
                    'Profesores por escenario de práctica',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20),
                  )),
            )
          ],
        ),
      ),
      drawer: MenuDrawer(),
    );
  }
}
