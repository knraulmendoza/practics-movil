import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:practics_movil/src/domain/entities/attention/AttentionEntity.dart';
import 'package:practics_movil/src/domain/entities/groupModule/GroupEntity.dart';
import 'package:practics_movil/src/domain/entities/person/PersonEntity.dart';
import 'package:practics_movil/src/domain/entities/procedure/ProcedureEntity.dart';
import 'package:practics_movil/src/domain/entities/student/StudentEntity.dart';
import 'package:practics_movil/src/helpers/Connection.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:practics_movil/src/services/GroupStudentService.dart';
import 'package:practics_movil/src/services/PersonService.dart';

class GlobalsController extends GetxController {
  Rx<PersonEntity> person = PersonEntity(
          documentType: null,
          document: null,
          firstName: null,
          secondName: null,
          firstLastName: null,
          secondLastName: null,
          phone: null,
          address: null,
          birthDay: null,
          gender: null)
      .obs;
  dynamic get currentUser =>
      jsonDecode(GetStorage().read('currentUser') ?? '{}');
  GroupEntity get group =>
      GroupEntity.fromJson(jsonDecode(GetStorage().read('group') ?? '{}'));
  Rx<dynamic> item = Object().obs;
  RxList<AttentionEntity> attentions = <AttentionEntity>[].obs;
  RxList<StudentEntity> studentsGroup = <StudentEntity>[].obs;
  RxBool connection = GetStorage().read<bool>('connection').obs ?? false.obs;
  // set currentUser(RxMap currentUser) =>
  //     this.currentUser = jsonDecode(GetStorage().read('currentUser') ?? '{}');

  @override
  void onInit() async {
    super.onInit();
    await getPerson();
  }

  void showProcedures(List<ProcedureEntity> procedures, String name) {
    Get.defaultDialog(
      radius: 20,
      title: "Procedimientos de $name",
      content: Container(
          height: Get.height * 0.14,
          width: 260,
          child: ListView.builder(
              itemBuilder: (_, index) {
                return Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Text(procedures[index].name));
              },
              itemCount: procedures.length)),
    );
  }

  Future<void> getStudents() async {
    if (await Connection.connection) {
      LoadingDialog.showLoadingDialog();
      final resp = await groupStudentService.getStudentGroupByTeacher();
      Get.back();
      this.studentsGroup.value = [];
      if (resp.data.state) {
        this.studentsGroup.value = (resp.data.data['studentsGroup'] as List)
            .map((student) => StudentEntity.fromJson(student))
            .toList();
      }
    } else {
      ResponseWidget.snackbar(
          title: 'Conexión',
          msg: 'No tienes conexión a internet',
          type: TypeDialog.ERROR);
    }
  }
  Future<void> getPerson() async {
    final storage = GetStorage();
    if (storage.read('authenticated')) {
        final resp = await personService.getPerson();
        if (resp.data.state) {
          final per = PersonEntity.fromJson(resp.data.data['person']);
          GetStorage().write('person', jsonEncode(per.toJsonSQlite()));
          this.person.value = per;
        } else {
          final person = GetStorage().read('person');
          this.person.value = PersonEntity.fromJsonSql(jsonDecode(person));
        } 
    }
  }
}
