import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:practics_movil/src/domain/enums/RolEnum.dart';

class MenuItem {
  final String title;
  Icon icon;
  final String page;
  final List<int> roles;
  MenuItem({this.title, this.icon, this.page, this.roles});
}

class MenuGlobalController extends GetxController {
  RxString currentPage = ''.obs;
  RxList<MenuItem> listAll = [
    MenuItem(
        title: 'Eventos',
        icon: Icon(
          Icons.event,
          color: Colors.white,
        ),
        roles: [
          RolEnum.ADMIN,
          RolEnum.STUDENT,
          RolEnum.TEACHER_PLANT,
          RolEnum.TEACHER_PRACTICT,
          RolEnum.COORDINATOR
        ],
        page: '/home'),
    MenuItem(
        title: 'Atención',
        icon: Icon(
          Icons.book,
          color: Colors.white,
        ),
        roles: [RolEnum.ADMIN, RolEnum.STUDENT],
        page: '/attention'),
    MenuItem(
        title: 'Historial',
        icon: Icon(
          Icons.calendar_today_rounded,
          color: Colors.white,
        ),
        roles: [RolEnum.ADMIN, RolEnum.STUDENT],
        page: '/history'),
    MenuItem(
        title: 'Rotaciones',
        icon: Icon(
          Icons.art_track,
          color: Colors.white,
        ),
        roles: [RolEnum.STUDENT],
        page: '/rotation'),
    MenuItem(
        title: 'Asistencia',
        icon: Icon(
          Icons.people,
          color: Colors.white,
        ),
        roles: [RolEnum.TEACHER_PRACTICT],
        page: '/assistent'),
    MenuItem(
        title: 'Atenciones',
        icon: Icon(
          Icons.book,
          color: Colors.white,
        ),
        roles: [RolEnum.TEACHER_PRACTICT],
        page: '/attentionList'),
    MenuItem(
        title: 'Pacientes',
        icon: Icon(
          Icons.person,
          color: Colors.white,
        ),
        roles: [RolEnum.ADMIN, RolEnum.TEACHER_PRACTICT],
        page: '/patient'),
    MenuItem(
        title: 'Rotaciones',
        icon: Icon(
          Icons.art_track,
          color: Colors.white,
        ),
        roles: [RolEnum.TEACHER_PRACTICT],
        page: '/rotationTeacher'),
    MenuItem(
        title: 'Chequeo a estudiante',
        icon: Icon(
          Icons.pages,
          color: Colors.white,
        ),
        roles: [RolEnum.TEACHER_PRACTICT],
        page: '/students'),
    MenuItem(
        title: 'Comparador chequeo',
        icon: Icon(
          Icons.pages,
          color: Colors.white,
        ),
        roles: [RolEnum.STUDENT, RolEnum.TEACHER_PRACTICT],
        page: '/comparatorCheck'),
    MenuItem(
        title: 'Profesores',
        icon: Icon(
          Icons.person,
          color: Colors.white,
        ),
        roles: [RolEnum.ADMIN, RolEnum.COORDINATOR],
        page: '/teachers'),
    MenuItem(
        title: 'Estudiantes',
        icon: Icon(
          Icons.person,
          color: Colors.white,
        ),
        roles: [RolEnum.ADMIN, RolEnum.COORDINATOR],
        page: '/studentsByScenaio'),
  ].obs;
  RxList<MenuItem> list = <MenuItem>[].obs;

  @override
  void onInit() {
    super.onInit();
  }

  void getCurrentPage() {
    final currentUser = jsonDecode(GetStorage().read('currentUser'));
    if (currentUser != null) {
      this.list.value = this
          .listAll
          .where(
              (item) => item.roles.contains((currentUser['rol']['id'] as int)))
          .toList();
      this.currentPage.value = this.list[0].page;
    }
  }

  bool currentPageSelected(int id) {
    this.list[id].icon = Icon(this.list[id].icon.icon,
        color: this.list[id].page == currentPage.value
            ? Colors.black
            : Colors.white);
    return this.list[id].page == currentPage.value ? true : false;
  }
}
