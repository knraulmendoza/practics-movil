import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:practics_movil/src/domain/entities/rol/RolEntity.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/menu/MenuDrawerController.dart';
import 'package:practics_movil/src/presentation/controllers/MenuGlobalController.dart';

class MenuDrawer extends GetWidget {
  @override
  Widget build(BuildContext context) {
    final c = Get.put(MenuDrawerController());
    final menuGlobal = Get.find<MenuGlobalController>();
    final global = Get.find<GlobalsController>();
    final currentUser = jsonDecode(GetStorage().read('currentUser'));
    final respRoles = jsonDecode(GetStorage().read('roles') ?? 'null');
    final roles = respRoles != null
        ? (respRoles as List).map((rol) => RolEntity.fromJson(rol)).toList()
        : [];
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          height: Get.height * 1,
          width: Get.width * 0.9,
          decoration: BoxDecoration(
              color: Colors.blue[700],
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(30),
                  topRight: Radius.circular(30))),
          child: SingleChildScrollView(
            child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 60),
                    width: Get.width * 0.8,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: 100,
                                alignment: Alignment.centerRight,
                                child: Image.asset(
                                  'assets/images/logo_blanco.png',
                                  fit: BoxFit.contain,
                                ),
                              ),
                              Container(
                                width: Get.width * 0.4,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      elevation: 0.0,
                                      primary: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(30),
                                        side: BorderSide(
                                            width: 0, color: Colors.white),
                                      )),
                                  child: Text(
                                    'Perfil',
                                    style: TextStyle(color: Colors.blue),
                                  ),
                                  onPressed: () {
                                    Get.toNamed('/perfil');
                                  },
                                ),
                              )
                            ],
                          ),
                          Obx(
                            () => Text(
                              global.person.value.fullName ?? '',
                              style:
                                  TextStyle(fontSize: 18, color: Colors.white),
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              style: TextStyle(fontSize: 18),
                              children: <TextSpan>[
                                TextSpan(
                                    text: '${currentUser['rol']['name']} de '),
                                TextSpan(
                                    text: '${currentUser['careerName']}',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: Get.height * 0.55,
                    child: SingleChildScrollView(
                      child: Column(
                        children: List<Widget>.generate(menuGlobal.list.length,
                            (index) {
                          return Container(
                            margin: menuGlobal.currentPageSelected(index)
                                ? EdgeInsets.symmetric(horizontal: 10)
                                : null,
                            decoration: BoxDecoration(
                                color: menuGlobal.currentPageSelected(index)
                                    ? Colors.white
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(20)),
                            child: ListTile(
                              title: Text(menuGlobal.list[index].title,
                                  style: TextStyle(
                                      color:
                                          menuGlobal.currentPageSelected(index)
                                              ? Colors.black
                                              : Colors.white)),
                              leading: menuGlobal.list[index].icon,
                              onTap: () {
                                menuGlobal.currentPage.value =
                                    menuGlobal.list[index].page;
                                Get.toNamed(menuGlobal.list[index].page);
                              },
                            ),
                          );
                        }),
                      ),
                    ),
                  ),
                  Obx(() => ListTile(
                        title: Text('Conexión',
                            style: TextStyle(
                              color: Colors.white,
                            )),
                        trailing: Switch(
                            activeColor: Colors.white,
                            focusColor: Colors.white,
                            value: global.connection.value,
                            onChanged: (value) async {
                              await GetStorage().write('connection', value);
                              global.connection.value = value;
                            }),
                      )),
                  ListTile(
                    title: Text(
                      'Cerrar sesión',
                      style: TextStyle(color: Colors.white),
                    ),
                    leading: Icon(Icons.exit_to_app, color: Colors.white),
                    onTap: () => c.logout(),
                  )
                ]),
          ),
        ),
        Align(
          alignment: Alignment(0, -0.85),
          child: GestureDetector(
            onTap: () {
              // onIconPressed();
              Get.back();
            },
            child: ClipPath(
              clipper: CustomMenuClipper(),
              child: Container(
                width: Get.width * 0.09,
                height: Get.height * 0.12,
                color: Colors.blue[700],
                alignment: Alignment.centerLeft,
                child: Icon(Icons.close, color: Colors.white),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class CustomMenuClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Paint paint = Paint();
    paint.color = Colors.white;

    final width = size.width;
    final height = size.height;

    Path path = Path();
    path.moveTo(0, 0);
    path.quadraticBezierTo(0, 8, 10, 16);
    path.quadraticBezierTo(width - 1, height / 2 - 20, width, height / 2);
    path.quadraticBezierTo(width + 1, height / 2 + 20, 10, height - 16);
    path.quadraticBezierTo(0, height - 8, 0, height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
