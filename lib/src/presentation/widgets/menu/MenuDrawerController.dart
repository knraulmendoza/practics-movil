import 'package:get/get.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/services/LoginService.dart';

class MenuDrawerController extends GetxController {
  final global = Get.find<GlobalsController>();
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() async {
    super.onReady();
  }

  void logout() async {
    LoadingDialog.showLoadingDialog(text: 'Cerrando sesión');
    final resp = await loginService.logout();
    Get.back();
    this.global.studentsGroup.value = [];
    loginService.deleteGetLocal();
    Get.offAllNamed('/login');
  }
}
