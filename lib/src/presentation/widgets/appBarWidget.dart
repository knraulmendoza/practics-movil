import 'package:flutter/material.dart';

class AppBarWidget extends StatelessWidget with PreferredSizeWidget  {
  final String title;
  final Widget leading;
  final List<Widget> icons;
  final Widget bottom;
  const AppBarWidget({Key key, this.title, this.icons, this.leading, this.bottom}) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
          elevation: 0.0,
          title: Text(this.title, style: TextStyle(color: Colors.blue),),
          actions: this.icons,
          bottom: this.bottom,
          leading: Builder(
          builder: (context) => this.leading ?? IconButton(
            icon: Icon(Icons.menu_rounded, color: Colors.blue,),
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
    );
  }
}