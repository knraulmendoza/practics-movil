import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/domain/enums/RolEnum.dart';
import 'package:practics_movil/src/domain/entities/attention/AttentionState.dart';
import 'package:practics_movil/src/domain/entities/procedure/ProcedureEntity.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/listAttention/ListAttentionController.dart';

class ListAttentionWidget extends GetView<ListAttentionController> {
  @override
  Widget build(BuildContext context) {
    Get.put(ListAttentionController());
    final global = Get.find<GlobalsController>();
    return Obx(() => Container(
          height: Get.height * 1,
          child: global.attentions.length == 0
              ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('No hay atenciones', textAlign: TextAlign.center),
                )
              : Obx(() => ListView.builder(
                    itemBuilder: (_, index) {
                      return ListTile(
                        leading: Text((index + 1).toString()),
                        title: Text(
                          global.attentions[index].patientEntity.name,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(
                          '${global.attentions[index].procedures.length} Procedimientos aplicados',
                          style: TextStyle(color: Colors.black),
                        ),
                        trailing: IconButton(
                            icon: Obx(
                              () => Icon(global.attentions.value[index].state ==
                                      AttentionState.ACCEPTED
                                  ? Icons.check_circle
                                  : Icons.check_circle_outline),
                            ),
                            color: Colors.green,
                            onPressed: global.currentUser['rol']['id'] ==
                                    RolEnum.STUDENT
                                ? () {}
                                : () => this
                                    .controller
                                    .updateAttentionCheck(index)),
                        onTap: () => this.showProcedures(
                            global.attentions[index].procedures,
                            global.attentions[index].patientEntity.name),
                        onLongPress:
                            global.currentUser['rol']['id'] == RolEnum.STUDENT
                                ? () {}
                                : () {},
                      );
                    },
                    itemCount: global.attentions.value.length,
                  )),
        ));
  }

  void showProcedures(List<ProcedureEntity> procedures, String name) {
    Get.defaultDialog(
      radius: 20,
      title: "Procedimientos de $name",
      content: Container(
          height: Get.height * 0.14,
          width: 260,
          child: ListView.builder(
              itemBuilder: (_, index) {
                return Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Text(procedures[index].name));
              },
              itemCount: procedures.length)),
    );
  }
}
