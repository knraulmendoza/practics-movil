import 'package:get/get.dart';
import 'package:practics_movil/src/domain/entities/attention/AttentionEntity.dart';
import 'package:practics_movil/src/domain/entities/attention/AttentionState.dart';
import 'package:practics_movil/src/helpers/Connection.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/presentation/widgets/LoadingWidget.dart';
import 'package:practics_movil/src/presentation/widgets/ResponseWidget.dart';
import 'package:practics_movil/src/services/AttentionService.dart';

class ListAttentionController extends GetxController {
  RxList<AttentionEntity> attentions = <AttentionEntity>[].obs;
  final global = Get.find<GlobalsController>();
  @override
  void onInit() async {
    super.onInit();
    // final resp = await AttentionService().getAttention();
    // if (resp.data.state) {
    //   this.attentions.value = (resp.data.data as List)
    //       .map((attention) => AttentionEntity.fromJson(attention))
    //       .toList();
    // }
  }

  void addAttention(AttentionEntity attentionEntity) {
    this.global.attentions.value.add(attentionEntity);
  }

  void updateAttentionCheck(int i) async {
    final attention = this.global.attentions[i];

    if (this.global.connection.value) {
      if (await Connection.connection) {
        LoadingDialog.showLoadingDialog();
        attention.state = attention.state == AttentionState.AWAIT
            ? AttentionState.ACCEPTED
            : AttentionState.AWAIT;
        final resp = await attentionService.updateAttention(attention);
        Get.back();
        if (!resp.data.state) {
          ResponseWidget.snackbar(
              title: 'Actualizando la atención',
              msg: resp.data.message,
              type: TypeDialog.ERROR);
          // attention.state = AttentionState.ACCEPTED;
        }
      } else {
        attention.state = attention.state == AttentionState.AWAIT
            ? AttentionState.ACCEPTED
            : AttentionState.AWAIT;
        ResponseWidget.snackbar(
            title: 'Conexión',
            msg: 'No tienes conexión a internet',
            type: TypeDialog.ERROR);
      }
    } else {
      ResponseWidget.snackbar(
          title: 'Conexión',
          msg: 'Debes habilitar la conexión desde el menú',
          type: TypeDialog.ERROR);
    }
  }
}
