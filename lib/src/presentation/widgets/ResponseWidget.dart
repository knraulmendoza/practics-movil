import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum TypeDialog { OK, ERROR }

class ResponseWidget {
  static void snackbar(
      {@required String title,
      @required String msg,
      @required TypeDialog type}) {
    Get.snackbar(title, msg,
        duration: Duration(seconds: 4),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor:
            type == TypeDialog.OK ? Colors.green[200] : Colors.red[200],
        margin: EdgeInsets.all(10));
  }
}
