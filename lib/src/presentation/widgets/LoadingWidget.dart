import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

class LoadingDialog {
  static void showLoadingDialog({String text}) async {
    // final spinkit = SpinKitSquareCircle(
    //   color: Colors.white,
    //   size: 50.0,
    //   controller: AnimationController(vsync: this, duration: const Duration(milliseconds: 1200)),
    // );
    await Get.dialog<void>(
      WillPopScope(
        onWillPop: () async => false,
        child: Container(
          height: Get.height * 1,
          width: Get.width * 1,
          color: Color.fromRGBO(0, 0, 0, 0.4),
          child: SimpleDialog(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            children: <Widget>[
              Center(
                  child: Column(mainAxisSize: MainAxisSize.max, children: [
                SpinKitWave(
                  color: Colors.blue,
                  size: 50,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  text ?? '',
                  style: TextStyle(color: Colors.blueAccent, fontSize: 16),
                )
              ]))
            ],
          ),
        ),
      ),
      barrierDismissible: false,
    );
  }
}
