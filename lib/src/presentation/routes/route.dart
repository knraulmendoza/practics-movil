import 'dart:convert';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:practics_movil/src/presentation/pages/Assistent/AssistentPage.dart';
import 'package:practics_movil/src/presentation/pages/AttentionList/AttentionListPage.dart';
import 'package:practics_movil/src/presentation/pages/changePassword/ChangePasswordPage.dart';
import 'package:practics_movil/src/presentation/pages/checks/ChecksPage.dart';
import 'package:practics_movil/src/presentation/pages/checksComparator/ChecksComparatorPage.dart';
import 'package:practics_movil/src/presentation/pages/encuesta/EncuestaPage.dart';
import 'package:practics_movil/src/presentation/pages/event/EventPage.dart';
import 'package:practics_movil/src/presentation/pages/historyAttentions/HistoryAttentionsPage.dart';
import 'package:practics_movil/src/presentation/pages/home/HomePage.dart';
import 'package:practics_movil/src/presentation/pages/attention/AttentionPage.dart';
import 'package:practics_movil/src/presentation/pages/login/LoginPage.dart';
import 'package:practics_movil/src/presentation/pages/menu/MenuPage.dart';
import 'package:practics_movil/src/presentation/pages/patient/PatientPage.dart';
import 'package:practics_movil/src/presentation/pages/perfil/PerfilPage.dart';
import 'package:practics_movil/src/presentation/pages/rotation/RotationPage.dart';
import 'package:practics_movil/src/presentation/pages/rotationTeacher/RotationTeacherPage.dart';
import 'package:practics_movil/src/presentation/pages/selectTeacher/SelectTeacherPage.dart';
import 'package:practics_movil/src/presentation/pages/students/StudentsPage.dart';
import 'package:practics_movil/src/presentation/pages/studentsByScenario/StudentsByScenarioPage.dart';
import 'package:practics_movil/src/presentation/pages/teachers/TeachersPage.dart';
import 'package:practics_movil/src/presentation/pages/teachersByScenario/TeachersByScenarioPage.dart';

List<GetPage> getAppRoutes() {
  final currentUser = GetStorage().read('currentUser') != null
      ? jsonDecode(GetStorage().read('currentUser'))
      : null;
  return [
    GetPage(
        name: '/',
        page: () => GetStorage().read('currentUser') == null ||
                GetStorage().read('token') == null
            ? LoginPage()
            : jsonDecode(GetStorage().read('currentUser'))['state_password'] ==
                    '0'
                ? ChangePasswordPage()
                : MenuPage()),
    GetPage(name: '/attention', page: () => AttentionPage()),
    GetPage(name: '/event', page: () => EventPage()),
    GetPage(name: '/change-password', page: () => ChangePasswordPage()),
    GetPage(name: '/login', page: () => LoginPage()),
    GetPage(name: '/home', page: () => HomePage()),
    GetPage(name: '/perfil', page: () => PerfilPage()),
    GetPage(name: '/history', page: () => HistoryAttentionsPage()),
    GetPage(name: '/rotation', page: () => RotationPage()),
    GetPage(name: '/attentionList', page: () => AttentionListPage()),
    GetPage(name: '/rotationTeacher', page: () => RotationTeacherPage()),
    GetPage(name: '/students', page: () => StudentsPage()),
    GetPage(name: '/studentsByScenaio', page: () => StudentByScenarioPage()),
    GetPage(name: '/teachers', page: () => SelectTeacherPage()),
    GetPage(name: '/teacher-by-document', page: () => TeachersPage()),
    GetPage(name: '/teacher-by-scenario', page: () => TeacherByScenarioPage()),
    GetPage(name: '/checks', page: () => ChecksPage()),
    GetPage(name: '/assistent', page: () => AssistentPage()),
    GetPage(name: '/patient', page: () => PatientPage()),
    GetPage(name: '/encuesta', page: () => EncuestaPage()),
    GetPage(name: '/comparatorCheck', page: () => ChecksComparatorPage()),
  ];
}
