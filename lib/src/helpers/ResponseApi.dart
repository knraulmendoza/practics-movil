import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:practics_movil/src/helpers/Status.dart';
import 'package:practics_movil/src/services/LoginService.dart';

class Data {
  bool _state;
  String _message;
  dynamic _data;
  bool get state => this._state;
  String get message => this._message;
  dynamic get data => this._data;
  set data(dynamic data) => this._data = data;
  Data(
      {@required bool state,
      @required String message,
      @required dynamic data}) {
    this._state = state;
    this._message = message;
    this._data = data;
  }

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
        state: json['state'] as bool,
        message: json['message'] as String,
        data: json['data']);
  }
}

class ResponseApi {
  int _status;
  Data _data;
  dynamic _dataError;
  int get status => this._status;
  Data get data => this._data;
  dynamic get dataError => this._dataError;
  set dataError(dynamic dataError) => this._dataError = dataError;

  ResponseApi({Response response}) {
    this._status = response.statusCode;
    if (response.statusCode == Status.UNAUTHORIZED) {
      // loginService.deleteGetLocal();
    } else {
      switch (response.statusCode) {
        case Status.REQUESTVALIDATIONS:
          this._dataError = response.data;
          this._data = Data.fromJson({"state": false, "message": this._dataError.toString(), "data": null}) ;
          break;
        case Status.ERRORSERVER:
          this._data = Data.fromJson({"state": false, "message": response.statusMessage, "data": null}) ;
          break;
        default:
          this._data = Data.fromJson(response.data);
      }
      // if (response.statusCode == Status.REQUESTVALIDATIONS) {
      //   this._dataError = response.data;
      // } else {
      //   this._data = Data.fromJson(response.data);
      // }
    }
  }
}
