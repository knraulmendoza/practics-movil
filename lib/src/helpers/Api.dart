import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:get_storage/get_storage.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';

class ApiBaseHelper {
  static const String baseUrl = 'http://practicassalud.unicesar.edu.co/api-practics';
  // static const String baseUrl = 'https://practicts.herokuapp.com';
  dynamic token() {
    final box = GetStorage();
    final currentUser = box.read('currentUser') != null
        ? jsonDecode(box.read('currentUser'))
        : null;
    return {
      HttpHeaders.authorizationHeader: 'Bearer ${box.read('token') ?? ''}',
      'rol': currentUser != null ? currentUser['rol']['id'] : 0,
      'career': currentUser != null ? currentUser['career'] : 0
    };
  }

  final dio = Dio(BaseOptions(
      baseUrl: '$baseUrl/api/',
      contentType: 'application/json',
      responseType: ResponseType.json,
      validateStatus: (_) => true));
  void ssl() {
     (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
  }
  Future<ResponseApi> httpGet(String url, {int page}) async {
    // ssl();
    final response = await dio.get(url,
        options: Options(headers: this.token()),
        queryParameters: {'page': page});
    return new ResponseApi(response: response);
  }

  Future<ResponseApi> httpPost(String url, dynamic body) async {
    // ssl();
    final response = await dio.post(url,
        data: body, options: Options(headers: this.token()));
    return new ResponseApi(response: response);
  }

  Future<dynamic> httpPut(String url, dynamic body) async {
    ssl();
    final response =
        await dio.put(url, data: body, options: Options(headers: this.token()));
    return new ResponseApi(response: response);
  }
}
