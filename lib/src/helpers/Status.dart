class Status {
  static const int OK = 200;
  static const int BADREQUEST = 400;
  static const int UNAUTHORIZED = 401;
  static const int REQUESTVALIDATIONS = 422;
  static const int ERRORSERVER = 500;
}
