import 'dart:io';

class Connection {
  static Future<bool> get connection async {
    bool state;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        state = true;
      }
    } on SocketException catch (_) {
      state = false;
    }
    return state;
  }
}
