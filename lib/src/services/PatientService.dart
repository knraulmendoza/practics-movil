import 'package:practics_movil/src/domain/entities/patient/PatientEntity.dart';
import 'package:practics_movil/src/helpers/Api.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';

class PatientService {
  ApiBaseHelper _api = new ApiBaseHelper();

  Future<ResponseApi> getPatient(int id) async {
    return await _api.httpGet('patient/$id');
  }

  Future<ResponseApi> updatePatient(int id, PatientEntity patient) async {
    return await _api.httpPut('patient/${id}', patient.toJsonUpdate());
  }
}

final patientService = new PatientService();
