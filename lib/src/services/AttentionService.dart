import 'dart:convert';

import 'package:get_storage/get_storage.dart';
import 'package:practics_movil/src/domain/entities/attention/AttentionEntity.dart';
import 'package:practics_movil/src/domain/entities/groupModule/GroupEntity.dart';
import 'package:practics_movil/src/helpers/Api.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';

class AttentionService {
  ApiBaseHelper _api = new ApiBaseHelper();

  Future<ResponseApi> addAttention(AttentionEntity attentionEntity) async {
    return await _api.httpPost(
        'attention', jsonEncode(attentionEntity.toJson()));
  }

  Future<ResponseApi> getAttention() async {
    return await _api.httpGet('attention');
  }

  Future<ResponseApi> getAttentionByDate(String date) async {
    return await _api.httpGet('attention/groupByDate/$date');
  }

  Future<ResponseApi> getAttentionByStudent(int studentId, String date) async {
    return await _api.httpGet('attention/attentionsByStudent/$studentId/$date');
  }

  Future<ResponseApi> getProcedures() async {
    final group =
        GroupEntity.fromJson(jsonDecode(GetStorage().read('group') ?? '{}'));
    return await _api
        .httpGet('procedure/getProdceduresByGroup/${group.moduleId}');
  }

  Future<ResponseApi> search(String document) async {
    return await _api.httpGet('patient/document/${document}');
  }

  Future<ResponseApi> updateAttention(AttentionEntity attentionEntity) async {
    return await _api.httpPut(
        'attention/${attentionEntity.id}', {'state': attentionEntity.state});
  }
}

final attentionService = new AttentionService();
