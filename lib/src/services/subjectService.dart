import 'package:practics_movil/src/domain/entities/asistent/AssistentEntity.dart';
import 'package:practics_movil/src/helpers/Api.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';

class SubjectService {
  ApiBaseHelper _api = new ApiBaseHelper();

  Future<ResponseApi> subjects() async {
    return await _api.httpGet('career/subjects');
  }
}

final subjectService = new SubjectService();
