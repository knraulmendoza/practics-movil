import 'package:practics_movil/src/domain/entities/check/CheckEntity.dart';
import 'package:practics_movil/src/domain/entities/poll/PollEntity.dart';
import 'package:practics_movil/src/helpers/Api.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';

class CheckService {
  ApiBaseHelper _api = new ApiBaseHelper();

  Future<ResponseApi> save(CheckEntity checkEntity, {int action}) async {
    return await _api.httpPost('check', checkEntity.toJson(action: action));
  }

  Future<ResponseApi> saveEncuesta(PollEntity pollEntity) async {
    return await _api.httpPost('check/saveEncuesta', pollEntity.toJson());
  }

  Future<ResponseApi> checks({int evaluated}) async {
    return await _api.httpGet('check/checks/${evaluated ?? ''}');
  }
  
  Future<ResponseApi> activeChecks({int id, int state}) async {
    return await _api.httpPost('check/activeCheck', { 'check': { 'id': id, 'state': state } });
  }
}

final checkService = new CheckService();
