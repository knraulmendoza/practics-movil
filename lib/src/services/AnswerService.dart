import 'package:practics_movil/src/domain/entities/answer/AnswerEntity.dart';
import 'package:practics_movil/src/helpers/Api.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';

class AnswerService {
  ApiBaseHelper _api = new ApiBaseHelper();

  Future<ResponseApi> save(List<AnswerEntity> answers, int evaluated) async {
    return await _api.httpPost('answer', {
      'evaluated': evaluated,
      'answers': answers.map((answer) => answer.toJson()).toList()
    });
  }

  Future<ResponseApi> saveAnswerEncuesta(
      List<AnswerEntity> answers, int attentionId) async {
    return await _api.httpPost('answer/saveAnswerEncuesta', {
      'attention_id': attentionId,
      'answers': answers.map((answer) => answer.toJsonPoll()).toList()
    });
  }
}

final answerService = new AnswerService();
