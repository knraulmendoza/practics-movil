import 'package:practics_movil/src/helpers/Api.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';

class QuestionService {
  ApiBaseHelper _api = new ApiBaseHelper();

  Future<ResponseApi> getQuestion() async {
    return await _api.httpGet('question/questionByRol');
  }

  Future<ResponseApi> questionEncuesta(data) async {
    return await _api.httpPost('question/questionsByCheckTypeAll', data);
  }
}

final questionService = new QuestionService();
