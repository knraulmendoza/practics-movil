import 'package:practics_movil/src/helpers/Api.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';

class EventService {
  ApiBaseHelper _api = new ApiBaseHelper();

  Future<ResponseApi> getEvent(int id) async {
    return await _api.httpGet('event/$id');
  }

  Future<ResponseApi> getEvents(int page) async {
    return await _api.httpGet('event', page: page);
  }
}

final eventService = new EventService();
