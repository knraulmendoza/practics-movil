import 'package:practics_movil/src/helpers/Api.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';

class GroupStudentService {
  ApiBaseHelper _api = new ApiBaseHelper();

  Future<ResponseApi> getGroup() async {
    return await _api.httpGet('groupStudent');
  }

  Future<ResponseApi> getStudentGroup(int groupId) async {
    return await _api.httpGet('groupStudent/studentsGroups/$groupId');
  }

  Future<ResponseApi> getStudentGroupByTeacher() async {
    return await _api.httpGet('groupStudent/studentsGroupsByTeacher');
  }
}

final groupStudentService = new GroupStudentService();
