import 'package:practics_movil/src/domain/entities/asistent/AssistentEntity.dart';
import 'package:practics_movil/src/helpers/Api.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';

class AssistenceService {
  ApiBaseHelper _api = new ApiBaseHelper();

  Future<ResponseApi> save(List<AssistenceEntity> assistences) async {
    return await _api.httpPost('assistence/list', {
      'assistences':
          assistences.map((assistence) => assistence.toJson()).toList()
    });
  }
}

final assistenceService = new AssistenceService();
