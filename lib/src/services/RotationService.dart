import 'package:practics_movil/src/helpers/Api.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';

class RotationService {
  ApiBaseHelper _api = new ApiBaseHelper();

  Future<ResponseApi> getRotations(int id) async {
    return await _api.httpGet('rotation/rotationsByGroup/$id');
  }

  Future<ResponseApi> getrotationsWithGroups() async {
    return await _api.httpGet('rotation/rotationsWithGroups');
  }

  Future<ResponseApi> scenarios() async {
    return await _api.httpGet('scenario');
  }
}

final rotationService = new RotationService();
