import 'package:practics_movil/src/domain/entities/person/PersonEntity.dart';
import 'package:practics_movil/src/helpers/Api.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';

class PersonService {
  ApiBaseHelper _api = new ApiBaseHelper();

  Future<ResponseApi> getPerson() async {
    return await _api.httpGet('person/getPerson');
  }

  Future<ResponseApi> updatePerson(PersonEntity personEntity) async {
    return await _api.httpPut(
        'person/${personEntity.id}', personEntity.toJsonUpdate());
  }

  Future<ResponseApi> teacherByDocument(String document) async {
    return await _api.httpGet('teacher/document/${document}');
  }

  Future<ResponseApi> teacherByScenario(int scenario) async {
    return await _api.httpGet('teacher/scenario/${scenario}');
  }

  Future<ResponseApi> studentByScenario(int scenario, { int subjecId }) async {
    return await _api.httpGet('student/scenario/${scenario}${subjecId != null ? "/"+subjecId.toString() : ""}');
  }
}

final personService = new PersonService();
