import 'dart:convert';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/data/service/AssistenceSerData.dart';
import 'package:practics_movil/src/data/service/AttentionSerData.dart';
import 'package:practics_movil/src/domain/entities/user/UserEntity.dart';
import 'package:practics_movil/src/domain/enums/RolEnum.dart';
import 'package:practics_movil/src/helpers/Api.dart';
import 'package:practics_movil/src/helpers/ResponseApi.dart';
import 'package:practics_movil/src/helpers/Status.dart';

class LoginService {
  ApiBaseHelper _api = new ApiBaseHelper();

  Future<ResponseApi> authenticate(UserEntity userEntity) async {
    return await _api.httpPost(
        'auth/authenticate', jsonEncode(userEntity.toJson()));
  }

  void validateToken() async {
    final resp = await _api.httpGet('auth/getToken');
    bool state = resp.status == Status.REQUESTVALIDATIONS ||
            resp.status == Status.BADREQUEST
        ? false
        : true;
    GetStorage().write('authenticated', state);
    if (!state) deleteGetLocal();
  }

  Future<ResponseApi> logout() async {
    return await _api.httpGet('auth/logout');
  }

  // Future<ResponseApi> updatedPassword(String newPassword) async {
  //   final storage = GetStorage();
  //   final currentUser = storage.read('currentUser')!=null?jsonDecode(storage.read('currentUser')):null;
  //   return await _api.httpPut("auth/${currentUser['id']}", {'password': newPassword});
  // }

  Future<ResponseApi> sendEmail(String user, String document) async {
    return await _api
        .httpPost('auth/email', {'user': user, 'document': document});
  }

  Future<ResponseApi> updatedPassword(String newPassword, String securityAnswer,
      {int userId, int securityQuestionId}) async {
    final storage = GetStorage();
    final currentUser = storage.read('currentUser') != null
        ? jsonDecode(storage.read('currentUser'))
        : null;
    return await _api.httpPut('auth/${userId ?? currentUser['id']}', {
      'password': newPassword,
      'security_question_id': securityQuestionId,
      'security_answer': securityAnswer,
    });
  }

  Future<ResponseApi> verifyUser(String user, String document,
      int securityQuestionId, String securityAnswer) async {
    return await _api.httpPost('auth/verifyUser', {
      'user': user,
      'document': document,
      'security_question_id': securityQuestionId,
      'security_answer': securityAnswer,
    });
  }

  Future<ResponseApi> securityQuestions() async {
    return await _api.httpGet('securityQuestion/getAll');
  }

  void deleteGetLocal() async {
    final storage = GetStorage();
    if (storage.read('currentUser') != null) {
      final currentUser = jsonDecode(storage.read('currentUser'));
      if (currentUser['rol']['id'] == RolEnum.TEACHER_PRACTICT)
        await assistenceSerData.deleting();
      if (currentUser['rol']['id'] == RolEnum.STUDENT)
        await attentionSerData.deleting();
    }
    GetStorage().write('date', Jiffy().format('yyy-MM-dd'));
    storage.remove('currentUser');
    storage.remove('token');
    storage.remove('group');
    storage.remove('person');
    storage.remove('authenticated');
  }
}

final loginService = new LoginService();
