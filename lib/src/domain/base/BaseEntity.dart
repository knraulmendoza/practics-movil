abstract class Entity {
  int _id;

  int get id => this._id;

  Entity(int id) {
    this._id = id;
  }

  Map<String, dynamic> toJson() {
    return {
      'id': this.id,
    };
  }

  Map<String, dynamic> toJsonSQlite() {
    return {
      'id': this.id,
    };
  }
}
