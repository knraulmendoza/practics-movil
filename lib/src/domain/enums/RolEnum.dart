class RolEnum {
  static const int ADMIN = 1;
  static const int COORDINATOR = 2;
  static const int TEACHER_PRACTICT = 3;
  static const int TEACHER_PLANT = 4;
  static const int STUDENT = 5;
}
