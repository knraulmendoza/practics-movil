import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';
import 'package:practics_movil/src/domain/entities/rol/RolEntity.dart';

class RxUserEntity {
  final RxString userName, password;
  final RxInt userParent, rolId;
  final RxList<RolEntity> roles;
  final Rx<RolEntity> role;

  RxUserEntity(
      {@required this.userName,
      @required this.password,
      this.userParent,
      this.rolId,
      this.role,
      this.roles});
}

class UserEntity extends Entity {
  RxUserEntity rxUserEntity;
  String get userName => this.rxUserEntity.userName.value;
  set userName(String userName) => this.rxUserEntity.userName.value = userName;
  String get password => this.rxUserEntity.password.value;
  set password(String password) => this.rxUserEntity.password.value = password;
  int get userParent => this.rxUserEntity.userParent.value;
  set userParent(int userParent) =>
      this.rxUserEntity.userParent.value = userParent;
  List<RolEntity> get roles => this.rxUserEntity.roles;
  set roles(List<RolEntity> roles) => this.rxUserEntity.roles.value = roles;
  RolEntity get role => this.rxUserEntity.role.value;
  int get rolId => this.rxUserEntity.rolId.value;
  set rolId(int rolId) => this.rxUserEntity.rolId.value = rolId;
  UserEntity(
      {int id,
      @required String userName,
      @required String password,
      int userParent,
      RolEntity role,
      List<RolEntity> roles})
      : super(id) {
    this.rxUserEntity = RxUserEntity(
        userName: userName.obs,
        password: password.obs,
        roles: roles.obs,
        role: role.obs,
        userParent: userParent.obs);
  }

  factory UserEntity.fromForm(Map<String, dynamic> json) {
    return UserEntity(
        userName: json['username'] as String,
        password: json['password'] as String,
        roles: []);
  }
  factory UserEntity.fromJson(Map<String, dynamic> json) {
    List<RolEntity> rolesAux = (json['user']['roles'] as List)
        .map((role) => RolEntity.fromJson(role))
        .toList();
    return UserEntity(
        id: json['user']['id'] as int,
        userName: json['user']['user'] as String,
        password: json['user']['password'] as String,
        userParent: json['user']['user_parent'] as int,
        roles: rolesAux); //List<RolEntity>.from(json['user']['roles']));
  }

  factory UserEntity.fromJsonSql(Map<String, dynamic> json) {
    return UserEntity(
        id: json['id'] as int,
        userName: json['user_name'] as String,
        password: json['password'] as String);
  }

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'user': this.userName,
      'password': this.password,
    };
    // map.addAll(super.toJson());

    return map;
  }

  @override
  Map<String, dynamic> toJsonSQlite() {
    Map<String, dynamic> map = {
      'user_name': this.userName,
      'password': this.password,
    };
    map.addAll(super.toJsonSQlite());
    return map;
  }
}
