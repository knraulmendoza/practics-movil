import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/data/dominio/Patient.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';
import 'package:practics_movil/src/domain/entities/attention/AttentionEntity.dart';
import 'package:practics_movil/src/domain/entities/person/DocumenteType.dart';
import 'package:practics_movil/src/domain/entities/person/Gender.dart';

class RxPatientEntity {
  final RxInt documentType,
      gender,
      age,
      status,
      academicLevel,
      employmentSituation;
  final RxString document, name, origin, phone;
  final RxList<AttentionEntity> attentions;

  RxPatientEntity(
      {@required this.documentType,
      this.gender,
      this.age,
      this.status,
      this.academicLevel,
      this.employmentSituation,
      @required this.document,
      @required this.name,
      this.origin,
      this.phone,
      this.attentions});
}

class PatientEntity extends Entity {
  RxPatientEntity rxPatientEntity;
  int get documentType => this.rxPatientEntity.documentType.value;
  set documentType(int documentType) =>
      this.rxPatientEntity.documentType.value = documentType;
  int get gender => this.rxPatientEntity.gender.value;
  set gender(int gender) => this.rxPatientEntity.gender.value = gender;
  int get age => this.rxPatientEntity.age.value;
  set age(int age) => this.rxPatientEntity.age.value = age;
  String get name => this.rxPatientEntity.name.value;
  set name(String name) => this.rxPatientEntity.name.value = name;
  String get origin => this.rxPatientEntity.origin.value;
  set origin(String origin) => this.rxPatientEntity.origin.value = origin;
  String get document => this.rxPatientEntity.document.value;
  set document(String document) =>
      this.rxPatientEntity.document.value = document;
  int get status => this.rxPatientEntity.status.value;
  set status(int status) => this.rxPatientEntity.status.value = status;
  int get academicLevel => this.rxPatientEntity.academicLevel.value;
  set academicLevel(int academicLevel) =>
      this.rxPatientEntity.academicLevel.value = academicLevel;
  int get employmentSituation => this.rxPatientEntity.employmentSituation.value;
  set employmentSituation(int employmentSituation) =>
      this.rxPatientEntity.employmentSituation.value = employmentSituation;
  String get phone => this.rxPatientEntity.phone.value;
  set phone(String phone) => this.rxPatientEntity.phone.value = phone;
  List<AttentionEntity> get attentions => this.rxPatientEntity.attentions.value;
  set attentions(List<AttentionEntity> attentions) =>
      this.rxPatientEntity.attentions.value = attentions;
  PatientEntity(
      {int id,
      int documentType = DocumentType.CEDULA,
      int gender,
      @required String document,
      @required String name,
      int age,
      String origin,
      int status,
      int academicLevel,
      int employmentSituation,
      List<AttentionEntity> attentions,
      String phone})
      : super(id) {
    this.rxPatientEntity = RxPatientEntity(
        documentType: documentType.obs,
        document: document.obs,
        name: name.obs,
        age: age.obs,
        academicLevel: academicLevel.obs,
        employmentSituation: employmentSituation.obs,
        gender: gender.obs,
        origin: origin.obs,
        phone: phone.obs,
        attentions:
            attentions == null ? <AttentionEntity>[].obs : attentions.obs,
        status: status.obs);
  }

  factory PatientEntity.fromJson(Map<String, dynamic> json) {
    return PatientEntity(
        id: json['id'] as int,
        document: json['document'] as String,
        documentType: int.parse(json['document_type']),
        name: json['name'] as String,
        age: json['age'] as int,
        status: json['status'] != null ? int.parse(json['status']) : json['status'],
        academicLevel: json['academic_level'] != null ? int.parse(json['academic_level']) : json['academic_level'],
        employmentSituation: json['employment_situation'] != null ? int.parse(json['employment_situation']) : json['employment_situation'],
        origin: json['origin'] as String,
        gender: json['gender'] != null ? int.parse(json['gender']) : json['gender'],
        phone: json['phone'] as String,
        attentions: json['attentions'] == null
            ? []
            : (json['attentions'] as List)
                .map((attention) => AttentionEntity.fromJson(attention))
                .toList());
  }
  factory PatientEntity.fromForm(Map<String, dynamic> json) {
    return PatientEntity(
        id: json['id'] as int,
        document: json['document'] as String,
        documentType: json['document_type'] as int,
        name: json['name'] as String,
        age: json['age'] as int,
        status: json['status'] as int,
        academicLevel: json['academicLevel'] as int,
        employmentSituation: json['employmentSituation'] as int,
        origin: json['origin'] as String,
        gender: json['gender'] as int,
        phone: json['phone'] as String);
  }

  factory PatientEntity.fromObjectHive(Patient patient) {
    return PatientEntity(
        document: patient.document,
        documentType: patient.documentType,
        name: patient.name,
        age: patient.age,
        status: patient.status,
        academicLevel: patient.academicLevel,
        employmentSituation: patient.emplotmentStituation,
        origin: patient.origin,
        gender: patient.gender,
        phone: patient.phone);
  }

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'document_type': this.documentType,
      'document': this.document,
      'name': this.name,
    };
    // map.addAll(super.toJson());

    return map;
  }

  @override
  Map<String, dynamic> toJsonUpdate() {
    Map<String, dynamic> map = {
      'age': this.age,
      'gender': this.gender,
      'status': this.status,
      'academic_level': this.academicLevel,
      'employment_situation': this.employmentSituation,
      'origin': this.origin,
      'phone': this.phone
    };
    return map;
  }

  String get genderText {
    switch (this.gender) {
      case Gender.MASCULINE:
        return Gender.MASCULINE_TEXT;
      case Gender.FEMENINE:
        return Gender.FEMENINE_TEXT;
    }
    return '';
  }

  String get documentTypeText {
    switch (this.documentType) {
      case DocumentType.CEDULA:
        return DocumentType.CEDULA_TEXT;
      case DocumentType.TARJETA_IDENTIDAD:
        return DocumentType.TARJETA_IDENTIDAD_TEXT;
      case DocumentType.CEDULA_EXTRANJERA:
        return DocumentType.CEDULA_EXTRANJERA_TEXT;
    }
    return '';
  }

  Patient toObjectHive() {
    return Patient()
      ..academicLevel = this.academicLevel
      ..age = this.age
      ..document = this.document
      ..documentType = this.documentType
      ..emplotmentStituation = this.employmentSituation
      ..gender = this.gender
      ..name = this.name
      ..origin = this.origin
      ..phone = this.phone
      ..status = this.status;
  }
}
