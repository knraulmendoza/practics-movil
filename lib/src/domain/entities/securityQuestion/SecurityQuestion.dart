import 'package:get/get.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';
import 'package:practics_movil/src/domain/entities/answer/AnswerEntity.dart';

class RxSecurityQuestionEntity {
  final RxString question;
  RxSecurityQuestionEntity({
    this.question,
  });
}

class SecurityQuestionEntity extends Entity {
  RxSecurityQuestionEntity rxSecurityQuestionEntity;
  String get question => this.rxSecurityQuestionEntity.question.value;
  SecurityQuestionEntity({int id, String question}) : super(id) {
    this.rxSecurityQuestionEntity = RxSecurityQuestionEntity(
      question: question.obs,
    );
  }

  factory SecurityQuestionEntity.fromJson(Map<String, dynamic> json) {
    return SecurityQuestionEntity(
      id: json['id'] as int,
      question: json['question'] as String,
    );
  }
}
