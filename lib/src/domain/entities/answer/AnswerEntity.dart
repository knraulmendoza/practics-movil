import 'package:get/get.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';

class RxAnswerEntity {
  final RxInt questionId, answer;
  final RxString observation;
  RxAnswerEntity({this.answer, this.observation, this.questionId});
}

class AnswerEntity extends Entity {
  RxAnswerEntity rxAnswerEntity;
  int get questionId => this.rxAnswerEntity.questionId.value;
  set questionId(int questionId) =>
      this.rxAnswerEntity.questionId.value = questionId;
  int get answer => this.rxAnswerEntity.answer.value;
  set answer(int answer) => this.rxAnswerEntity.answer.value = answer;
  String get observation => this.rxAnswerEntity.observation.value;
  set observation(String observation) =>
      this.rxAnswerEntity.observation.value = observation;

  AnswerEntity({int id, int questionId, int answer, String observation})
      : super(id) {
    this.rxAnswerEntity = RxAnswerEntity(
        answer: answer.obs,
        observation: observation.obs,
        questionId: questionId.obs);
  }

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'question_id': this.questionId,
      'answer': this.answer,
      'observation': this.observation ?? ' '
    };
    // map.addAll(super.toJson());
    return map;
  }

  @override
  Map<String, dynamic> toJsonPoll() {
    Map<String, dynamic> map = {
      'question_id': this.questionId,
      'answer': this.answer,
    };
    // map.addAll(super.toJson());
    return map;
  }

  @override
  factory AnswerEntity.fromJson(Map<String, dynamic> json) {
    return AnswerEntity(
        id: json['id'] as int,
        answer: json['answer'],
        observation: json['observation'] == null ? '' : json['observation']);
  }
}
