import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';
import 'package:practics_movil/src/domain/entities/person/PersonEntity.dart';

class RxTeacherEntity {
  final Rx<PersonEntity> personEntity;
  final RxInt checks, checksByRotation;
  RxTeacherEntity({this.personEntity, this.checks, this.checksByRotation});
}

class TeacherEntity extends Entity {
  RxTeacherEntity rxTeacherEntity;
  PersonEntity get personEntity => this.rxTeacherEntity.personEntity.value;
  int get checks => this.rxTeacherEntity.checks.value ?? 0;
  int get checksByRotations => this.rxTeacherEntity.checks.value ?? 0;
  set checks(int checks) =>
      this.rxTeacherEntity.checks.value = this.checks + checks;
  TeacherEntity(
      {int id,
      @required PersonEntity personEntity,
      int checks,
      int checksByRotations})
      : super(id) {
    this.rxTeacherEntity = RxTeacherEntity(
        personEntity: personEntity.obs,
        checks: checks.obs,
        checksByRotation: checksByRotations.obs);
  }

  factory TeacherEntity.fromJson(Map<String, dynamic> json) {
    return TeacherEntity(
        id: json['id'] as int,
        personEntity: PersonEntity.fromJson(json['person']),
        checks: (json['checks'] as int) ?? 0,
        checksByRotations: (json['checksByRotation'] as int) ?? 0);
  }
}
