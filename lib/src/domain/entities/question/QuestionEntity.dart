import 'package:get/get.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';
import 'package:practics_movil/src/domain/entities/answer/AnswerEntity.dart';

class RxQuestionEntity {
  final RxInt type;
  final RxString question, checkType;
  final RxList<AnswerEntity> answers;
  RxQuestionEntity({this.checkType, this.question, this.type, this.answers});
}

class QuestionEntity extends Entity {
  RxQuestionEntity rxQuestionEntity;
  String get question => this.rxQuestionEntity.question.value;
  String get checkType => this.rxQuestionEntity.checkType.value;
  int get type => this.rxQuestionEntity.type.value;
  List<AnswerEntity> get answers => this.rxQuestionEntity.answers.value;
  QuestionEntity(
      {int id,
      String question,
      String checkType,
      int type,
      List<AnswerEntity> answers})
      : super(id) {
    this.rxQuestionEntity = RxQuestionEntity(
        checkType: checkType.obs,
        question: question.obs,
        type: type.obs,
        answers: answers.obs);
  }

  factory QuestionEntity.fromJson(Map<String, dynamic> json) {
    return QuestionEntity(
        id: json['id'] as int,
        checkType: json['check_type'] as String,
        question: json['question'] as String,
        type: int.parse(json['type']),
        answers: []);
  }

  factory QuestionEntity.fromJsonWithAnswers(Map<String, dynamic> json) {
    return QuestionEntity(
        id: json['id'] as int,
        checkType: json['check_type'] as String,
        question: json['question'] as String,
        type: int.parse(json['type']),
        answers: (json['answers'] as List)
            .map((answer) => AnswerEntity.fromJson(answer))
            .toList());
  }
}
