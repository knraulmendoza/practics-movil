import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';
import 'package:practics_movil/src/helpers/Api.dart';

class RxEventEntity {
  final RxString title, description, address, image;
  final Rx<DateTime> date;
  final RxInt id;
  RxEventEntity(
      {this.id,
      this.title,
      this.description,
      this.address,
      this.date,
      this.image});
}

class EventEntity extends Entity {
  RxEventEntity rxEventEntity;
  String get title => this.rxEventEntity.title.value;
  String get description => this.rxEventEntity.description.value;
  String get address => this.rxEventEntity.address.value;
  String get image => this.rxEventEntity.image.value;
  DateTime get date => this.rxEventEntity.date.value;
  EventEntity(
      {int id,
      String title,
      String description,
      String address,
      String image,
      DateTime date})
      : super(id) {
    this.rxEventEntity = RxEventEntity(
        id: id.obs,
        address: address.obs,
        date: date.obs,
        description: description.obs,
        title: title.obs,
        image: image.obs);
  }

  factory EventEntity.fromJson(Map<String, dynamic> json) {
    return EventEntity(
        id: json['id'],
        title: json['title'],
        description: json['description'],
        image: '${ApiBaseHelper.baseUrl}/images/${json['image']}',
        date: Jiffy('${json['date']} ${json['hour']}').dateTime);
  }
}
