import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';
import 'package:practics_movil/src/domain/entities/person/PersonEntity.dart';

class RxStudentEntity {
  final Rx<PersonEntity> personEntity;
  final RxInt checks, attentions;
  RxStudentEntity({this.personEntity, this.checks, this.attentions});
}

class StudentEntity extends Entity {
  RxStudentEntity rxStudentEntity;
  PersonEntity get personEntity => this.rxStudentEntity.personEntity.value;
  int get checks => this.rxStudentEntity.checks.value ?? 0;
  int get attentions => this.rxStudentEntity.attentions.value ?? 0;
  set checks(int checks) =>
      this.rxStudentEntity.checks.value = this.checks + checks;
  StudentEntity(
      {int id, @required PersonEntity personEntity, int checks, int attentions})
      : super(id) {
    this.rxStudentEntity = RxStudentEntity(
        personEntity: personEntity.obs,
        checks: checks.obs,
        attentions: attentions.obs);
  }

  factory StudentEntity.fromJson(Map<String, dynamic> json) {
    return StudentEntity(
        id: json['id'] as int,
        personEntity: PersonEntity.fromJson(json['person']),
        checks: (json['checks'] as int) ?? 0,
        attentions: (json['attentions'] as int) ?? 0);
  }
}
