import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/data/dominio/Procedure.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';
import 'package:practics_movil/src/domain/entities/module/ModuleEntity.dart';

class RxProcedureEntity {
  final RxString name, code, description;
  final Rx<ModuleEntity> moduleEntity;

  RxProcedureEntity(
      {@required this.name,
      @required this.code,
      @required this.description,
      @required this.moduleEntity});
}

class ProcedureEntity extends Entity {
  RxProcedureEntity rxProcedureEntity;
  String get name => this.rxProcedureEntity.name.value;
  String get code => this.rxProcedureEntity.code.value;
  String get description => this.rxProcedureEntity.description.value;
  ModuleEntity get moduleEntity => this.rxProcedureEntity.moduleEntity.value;
  ProcedureEntity(
      {int id,
      @required String name,
      @required String code,
      @required String description,
      ModuleEntity moduleEntity})
      : super(id) {
    this.rxProcedureEntity = RxProcedureEntity(
        name: name.obs,
        code: code.obs,
        description: description.obs,
        moduleEntity: moduleEntity.obs);
  }

  factory ProcedureEntity.fromJson(Map<String, dynamic> json) {
    return ProcedureEntity(
      id: json['id'] as int,
      name: json['name'] as String,
      code: json['code'] as String,
      description: json['description'] as String,
      // moduleEntity: ModuleEntity.fromJson(json['module']),
    );
  }
  factory ProcedureEntity.fromObjectHive(Procedure procedure) {
    return ProcedureEntity(
        name: procedure.name,
        code: procedure.code,
        description: procedure.description
        // moduleEntity: ModuleEntity.fromJson(json['module']),
        );
  }

  Procedure toObjectHive() {
    return Procedure()
      ..code = this.code
      ..description = this.description
      ..name = this.name;
  }
}
