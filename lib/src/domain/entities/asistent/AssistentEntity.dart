import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/data/dominio/Assistence.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';

class RxAssistenceEntity {
  final RxInt assist, studentId;
  final RxString observation;
  RxAssistenceEntity({this.assist, this.observation, this.studentId});
}

class AssistenceEntity extends Entity {
  RxAssistenceEntity rxAssistenceEntity;
  int get assist => this.rxAssistenceEntity.assist.value;
  set assist(int assist) => this.rxAssistenceEntity.assist.value = assist;
  int get studentId => this.rxAssistenceEntity.studentId.value;
  set studentId(int studentId) =>
      this.rxAssistenceEntity.studentId.value = studentId;
  String get observation => this.rxAssistenceEntity.observation.value;
  set observation(String observation) =>
      this.rxAssistenceEntity.observation.value = observation;

  AssistenceEntity({int id, int assist, int studentId, String observation})
      : super(id) {
    this.rxAssistenceEntity = RxAssistenceEntity(
        assist: assist.obs,
        observation: observation.obs,
        studentId: studentId.obs);
  }

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'student_id': this.studentId,
      'assist': this.assist,
      'observation': this.observation
    };
    // map.addAll(super.toJson());
    return map;
  }

  Assistence toObjectHive() {
    return Assistence()
      ..assist = this.assist
      ..studentId = this.studentId
      ..observation = this.observation
      ..date = Jiffy().format('yyy-MM-dd');
  }

  factory AssistenceEntity.fromObjectHive(Assistence assistence) {
    return AssistenceEntity()
      ..assist = assistence.assist
      ..studentId = assistence.studentId
      ..observation = assistence.observation;
  }
}
