import 'package:get/get.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';
import 'package:practics_movil/src/domain/entities/student/StudentEntity.dart';

class RxGroupEntity {
  final RxString code, name;
  final RxInt moduleId, id;
  final RxList<StudentEntity> students;
  RxGroupEntity({this.id, this.code, this.name, this.moduleId, this.students});
}

class GroupEntity extends Entity {
  RxGroupEntity rxGroupEntity;
  String get code => this.rxGroupEntity.code.value;
  String get name => this.rxGroupEntity.name.value;
  int get moduleId => this.rxGroupEntity.moduleId.value;
  List<StudentEntity> get students => this.rxGroupEntity.students.value;
  GroupEntity(
      {int id,
      String name,
      String code,
      int moduleId,
      List<StudentEntity> students})
      : super(id) {
    this.rxGroupEntity = RxGroupEntity(
        code: code.obs,
        id: id.obs,
        moduleId: moduleId.obs,
        name: name.obs,
        students: students.obs);
  }

  factory GroupEntity.fromJsonWithStudents(Map<String, dynamic> json) {
    return GroupEntity(
        id: json['id'],
        code: json['code'],
        moduleId: json['module_id'],
        name: json['name'],
        students: (json['students'] as List)
            .map((student) => StudentEntity.fromJson(student))
            .toList());
  }

  factory GroupEntity.fromJson(Map<String, dynamic> json) {
    return GroupEntity(
        id: json['id'],
        code: json['code'],
        moduleId: json['module_id'],
        name: json['name'],
        students: []);
  }

  @override
  Map<String, dynamic> toJsonSQlite() {
    Map<String, dynamic> map = {
      'id': this.id,
      'code': this.code,
      'module_id': this.moduleId,
      'name': this.name
    };
    map.addAll(super.toJsonSQlite());
    return map;
  }
}
