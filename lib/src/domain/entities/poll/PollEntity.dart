import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';

class RxPollEntity {
  final RxInt evaluated, attentionId;
  final Rx<DateTime> date;
  RxPollEntity({this.evaluated, this.date, this.attentionId});
}

class PollEntity extends Entity {
  RxPollEntity rxPollEntity;
  int get evaluated => this.rxPollEntity.evaluated.value;
  set evaluated(int evaluated) => this.rxPollEntity.evaluated.value = evaluated;
  int get attentionId => this.rxPollEntity.attentionId.value;
  set attentionId(int attentionId) =>
      this.rxPollEntity.attentionId.value = attentionId;
  DateTime get date => this.rxPollEntity.date.value;
  set date(DateTime date) => this.rxPollEntity.date.value = date;
  PollEntity({int id, int evaluated, @required int attentionId, DateTime date})
      : super(id) {
    this.rxPollEntity = RxPollEntity(
      date: date.obs,
      attentionId: attentionId.obs,
      evaluated: evaluated.obs,
    );
  }
  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'attention_id': this.attentionId,
      'date': Jiffy().format('yyy-MM-dd')
    };
    // map.addAll(super.toJson());
    return map;
  }

  @override
  factory PollEntity.fromJson(Map<String, dynamic> json) {
    return PollEntity(
        id: json['id'] as int,
        evaluated: json['evaluated'],
        date: json['date']);
  }
}
