import 'package:get/get.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';
import 'package:practics_movil/src/domain/entities/groupModule/GroupEntity.dart';

class RxRotationEntity {
  final RxString stage, area, workingDay;
  final Rx<DateTime> date;
  final RxBool state;
  final Rx<GroupEntity> group;
  RxRotationEntity(
      {this.date,
      this.stage,
      this.area,
      this.workingDay,
      this.state,
      this.group});
}

class RotationEntity extends Entity {
  RxRotationEntity rxRotationEntity;
  DateTime get date => this.rxRotationEntity.date.value;
  bool get state => this.rxRotationEntity.state.value;
  String get stage => this.rxRotationEntity.stage.value;
  String get area => this.rxRotationEntity.area.value;
  String get workinDay => this.rxRotationEntity.workingDay.value;
  GroupEntity get group => this.rxRotationEntity.group.value;
  RotationEntity(
      {int id,
      DateTime date,
      bool state,
      String stage,
      String area,
      String workingDay,
      GroupEntity group})
      : super(id) {
    this.rxRotationEntity = RxRotationEntity(
        date: date.obs,
        state: state.obs,
        area: area.obs,
        stage: stage.obs,
        workingDay: workingDay.obs,
        group: group.obs);
  }

  factory RotationEntity.fromJson(Map<String, dynamic> json) {
    return RotationEntity(
      date: DateTime.parse(json['end_date']),
      state: json['state'] as bool,
      area: json['area'] as String,
      stage: json['escenario'] as String,
      workingDay: json['working_day'] as String,
      // group: GroupEntity.fromJsonWithStudents(json['group'])
    );
  }

  factory RotationEntity.fromJsonWithGroup(Map<String, dynamic> json) {
    return RotationEntity(
        date: DateTime.parse(json['end_date']),
        state: json['state'] as bool,
        area: json['area'] as String,
        stage: json['escenario'] as String,
        workingDay: json['working_day'] as String,
        group: GroupEntity.fromJsonWithStudents(json['group']));
  }
}
