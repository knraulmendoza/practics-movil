import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/data/dominio/Attention.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';
import 'package:practics_movil/src/domain/entities/attention/AttentionState.dart';
import 'package:practics_movil/src/domain/entities/patient/PatientEntity.dart';
import 'package:practics_movil/src/domain/entities/procedure/ProcedureEntity.dart';

class RxAttentionEntity {
  final Rx<DateTime> date;
  final RxInt patientId;
  final RxString state;
  final Rx<PatientEntity> patientEntity;
  final RxList<ProcedureEntity> procedures;
  final RxList<int> proceduresId;
  final RxBool sent;

  RxAttentionEntity(
      {@required this.date,
      this.state,
      this.patientId,
      @required this.patientEntity,
      @required this.procedures,
      this.proceduresId,
      this.sent});
}

class AttentionEntity extends Entity {
  RxAttentionEntity rxAttentionEntity;
  DateTime get date => this.rxAttentionEntity.date.value;
  set date(DateTime date) => this.rxAttentionEntity.date.value = date;
  String get state => this.rxAttentionEntity.state.value;
  set state(String state) => this.rxAttentionEntity.state.value = state;
  List<ProcedureEntity> get procedures =>
      this.rxAttentionEntity.procedures.value;
  set procedures(List<ProcedureEntity> procedures) =>
      this.rxAttentionEntity.procedures.value = procedures;
  List<int> get proceduresId => this.rxAttentionEntity.proceduresId.value;
  set proceduresId(List<int> proceduresId) =>
      this.rxAttentionEntity.proceduresId.value = proceduresId;
  int get patientId => this.rxAttentionEntity.patientId.value;
  PatientEntity get patientEntity => this.rxAttentionEntity.patientEntity.value;
  set patientEntity(PatientEntity patientEntity) =>
      this.rxAttentionEntity.patientEntity.value = patientEntity;
  bool get sent => this.rxAttentionEntity.sent.value;
  set sent(bool sent) => this.rxAttentionEntity.sent.value = sent;
  AttentionEntity(
      {int id,
      @required DateTime date,
      String state,
      List<ProcedureEntity> procedures,
      List<int> proceduresId,
      int patientId,
      bool sent,
      @required PatientEntity patientEntity})
      : super(id) {
    this.rxAttentionEntity = RxAttentionEntity(
        date: date.obs,
        patientEntity: patientEntity.obs,
        procedures: procedures.obs,
        state: state.obs,
        patientId: patientId.obs,
        sent: sent.obs,
        proceduresId: proceduresId == null ? <int>[].obs : proceduresId.obs);
  }

  factory AttentionEntity.fromJson(Map<String, dynamic> json) {
    List<dynamic> list = json['procedures'];
    return AttentionEntity(
        id: json['id'] as int,
        date: DateTime.parse(json['date']),
        state: json['state'],
        patientId: json['patient_id'],
        procedures:
            list.map((element) => ProcedureEntity.fromJson(element)).toList(),
        patientEntity: json['patient'] == null
            ? PatientEntity(document: '', name: '', attentions: [])
            : PatientEntity.fromJson(json['patient']));
  }

  factory AttentionEntity.fromObjectHive(Attention attention) {
    return AttentionEntity(
        date: DateTime.parse(attention.date),
        state: attention.state,
        // patientId: attention.,
        proceduresId: attention.proceduresId,
        sent: attention.stateSent,
        procedures: attention.procedures
            .map((procedure) => ProcedureEntity.fromObjectHive(procedure))
            .toList(),
        patientEntity: PatientEntity.fromObjectHive(attention.patient));
  }

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'date': Jiffy().format('yyy-MM-dd'),
      'procedures': this.proceduresId,
      'patient': this.patientEntity.toJson()
    };
    // map.addAll(super.toJson());
    return map;
  }

  Attention toObjectHive() {
    return Attention()
      ..date = Jiffy().format('yyy-MM-dd')
      ..patient = this.patientEntity.toObjectHive()
      ..state = this.state != null ? this.state:  AttentionState.AWAIT
      ..proceduresId = this.proceduresId
      ..procedures =
          this.procedures.map((procedure) => procedure.toObjectHive()).toList();
  }
}
