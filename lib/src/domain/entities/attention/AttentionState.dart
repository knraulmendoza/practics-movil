class AttentionState {
  static const String AWAIT = '0';
  static const String ACCEPTED = '1';
  static const String EVALUATED = '2';
}
