import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';

class RxCheckEntity {
  final RxInt evaluated, evaluator;
  final Rx<DateTime> date;
  final RxString observation;
  RxCheckEntity({this.evaluated, this.evaluator, this.date, this.observation});
}

class CheckEntity extends Entity {
  RxCheckEntity rxCheckEntity;
  int get evaluated => this.rxCheckEntity.evaluated.value;
  set evaluated(int evaluated) =>
      this.rxCheckEntity.evaluated.value = evaluated;
  int get evaluator => this.rxCheckEntity.evaluator.value;
  set evaluator(int evaluator) =>
      this.rxCheckEntity.evaluator.value = evaluator;
  DateTime get date => this.rxCheckEntity.date.value;
  set date(DateTime date) => this.rxCheckEntity.date.value = date;
  String get observation => this.rxCheckEntity.observation.value;
  set observation(String observation) =>
      this.rxCheckEntity.observation.value = observation;
  CheckEntity(
      {int id,
      @required int evaluated,
      @required int evaluator,
      @required DateTime date,
      String observation})
      : super(id) {
    this.rxCheckEntity = RxCheckEntity(
        date: date.obs,
        evaluated: evaluated.obs,
        evaluator: evaluator.obs,
        observation: observation.obs);
  }
  @override
  Map<String, dynamic> toJson({int action = 1}) {
    Map<String, dynamic> map = {
      'evaluated': this.evaluated,
      'evaluator': this.evaluator,
      'date': Jiffy().format('yyy-MM-dd'),
      'observation': this.observation,
      'action': action
    };
    // map.addAll(super.toJson());
    return map;
  }

  @override
  factory CheckEntity.fromJson(Map<String, dynamic> json) {
    return CheckEntity(
        id: json['id'] as int,
        evaluated: json['evaluated'],
        evaluator: json['evaluated'],
        date: json['date'],
        observation: json['observation']);
  }
}
