import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';
import 'package:practics_movil/src/domain/entities/person/DocumenteType.dart';
import 'package:practics_movil/src/domain/entities/person/Gender.dart';

class RxPersonEntity {
  final RxString document,
      firstName,
      secondName,
      firstLastName,
      secondLastName,
      phone,
      address;
  final Rx<DateTime> birthDay;
  final RxInt gender, documentType;

  RxPersonEntity(
      {@required this.document,
      @required this.firstName,
      @required this.secondName,
      @required this.firstLastName,
      @required this.secondLastName,
      @required this.phone,
      @required this.address,
      @required this.birthDay,
      @required this.gender,
      @required this.documentType});
}

class PersonEntity extends Entity {
  RxPersonEntity rxPersonEntity;
  int get documentType => this.rxPersonEntity.documentType.value;
  String get document => this.rxPersonEntity.document.value ?? '';
  String get firstName => this.rxPersonEntity.firstName.value ?? '';
  String get secondName => this.rxPersonEntity.secondName.value ?? '';
  String get firstLastName => this.rxPersonEntity.firstLastName.value ?? '';
  String get secondLastName => this.rxPersonEntity.secondLastName.value ?? '';
  String get phone => this.rxPersonEntity.phone.value ?? '';
  set phone(String phone) => this.rxPersonEntity.phone.value = phone;
  String get address => this.rxPersonEntity.address.value ?? '';
  set address(String address) => this.rxPersonEntity.address.value = address;
  DateTime get birthDay => this.rxPersonEntity.birthDay.value;
  int get gender => this.rxPersonEntity.gender.value;
  set gender(int gender) => this.rxPersonEntity.gender.value = gender;
  PersonEntity(
      {int id,
      @required int documentType,
      @required String document,
      @required String firstName,
      @required String secondName,
      @required String firstLastName,
      @required String secondLastName,
      @required String phone,
      @required String address,
      @required DateTime birthDay,
      @required int gender})
      : super(id) {
    this.rxPersonEntity = RxPersonEntity(
        document: document.obs,
        firstName: firstName.obs,
        secondName: secondName.obs,
        firstLastName: firstLastName.obs,
        secondLastName: secondLastName.obs,
        phone: phone.obs,
        address: address.obs,
        birthDay: birthDay.obs,
        gender: gender.obs,
        documentType: documentType.obs);
  }
  String get fullName =>
      '${this.firstName} ${this.secondName ?? ''} ${this.firstLastName} ${this.secondLastName}';
  factory PersonEntity.fromJson(Map<String, dynamic> json) {
    return PersonEntity(
        id: json['id'] as int,
        documentType: int.parse(json['document_type']),
        document: json['document'] as String,
        firstName: json['first_name'] as String,
        secondName: json['second_name'] as String,
        firstLastName: json['first_last_name'] as String,
        address: json['address'] as String,
        secondLastName: json['second_last_name'] as String,
        birthDay: json['birth_day'] as DateTime,
        gender: int.parse(json['gender']),
        phone: json['phone'] as String);
  }

  factory PersonEntity.fromJsonSql(Map<String, dynamic> json) {
    return PersonEntity(
        id: json['id'] as int,
        documentType: json['document_type'] as int,
        document: json['document'] as String,
        firstName: json['first_name'] as String,
        secondName: json['second_name'] as String,
        firstLastName: json['first_last_name'] as String,
        address: json['address'] as String,
        secondLastName: json['second_last_name'] as String,
        birthDay: json['birth_day'] as DateTime,
        gender: json['gender'] as int,
        phone: json['phone'] as String);
  }
  @override
  Map<String, dynamic> toJsonUpdate() {
    Map<String, dynamic> map = {
      'address': this.address,
      'gender': this.gender,
      'phone': this.phone
    };
    return map;
  }

  @override
  Map<String, dynamic> toJsonSQlite() {
    Map<String, dynamic> map = {
      'document_type': this.documentType,
      'document': this.document,
      'first_name': this.firstName,
      'second_name': this.secondName,
      'first_last_name': this.secondLastName,
      'second_last_name': this.secondLastName,
      'address': this.address,
      'gender': this.gender,
      'phone': this.phone,
      'birth_day': this.birthDay
    };
    map.addAll(super.toJsonSQlite());
    return map;
  }

  String get genderText {
    switch (this.gender) {
      case Gender.MASCULINE:
        return Gender.MASCULINE_TEXT;
      case Gender.FEMENINE:
        return Gender.FEMENINE_TEXT;
    }
    return '';
  }

  String get documentTypeText {
    switch (this.documentType) {
      case DocumentType.CEDULA:
        return DocumentType.CEDULA_TEXT;
      case DocumentType.TARJETA_IDENTIDAD:
        return DocumentType.TARJETA_IDENTIDAD_TEXT;
      case DocumentType.CEDULA_EXTRANJERA:
        return DocumentType.CEDULA_EXTRANJERA_TEXT;
    }
    return '';
  }
}
