class DocumentType {
  static const int CEDULA = 1;
  static const int TARJETA_IDENTIDAD = 2;
  static const int CEDULA_EXTRANJERA = 3;
  static const String CEDULA_TEXT = 'cedula';
  static const String TARJETA_IDENTIDAD_TEXT = 'tarjeta de identidad';
  static const String CEDULA_EXTRANJERA_TEXT = 'cedula extranjera';
}
