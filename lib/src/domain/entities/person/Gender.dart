class Gender {
  static const String MASCULINE_TEXT = 'masculino';
  static const String FEMENINE_TEXT = 'femenino';
  static const int MASCULINE = 1;
  static const int FEMENINE = 2;
}
