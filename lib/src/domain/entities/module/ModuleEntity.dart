import 'package:get/get.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';

class RxModuleEntity {
  final RxString name, code, description;
  final RxInt subjectId;

  RxModuleEntity({this.name, this.code, this.description, this.subjectId});
}

class ModuleEntity extends Entity {
  RxModuleEntity rxModuleEntity;
  String get name => this.rxModuleEntity.name.value;
  String get code => this.rxModuleEntity.code.value;
  String get description => this.rxModuleEntity.description.value;
  int get subjectId => this.rxModuleEntity.subjectId.value;

  ModuleEntity(
      {int id, String name, String code, String description, int subjectId})
      : super(id) {
    this.rxModuleEntity = RxModuleEntity(
        name: name.obs,
        code: code.obs,
        description: description.obs,
        subjectId: subjectId.obs);
  }

  factory ModuleEntity.fromJson(Map<String, dynamic> json) {
    return ModuleEntity(
      id: json['id'] as int,
      name: json['name'] as String,
      code: json['code'] as String,
      description: json['description'] as String,
      subjectId: json['subject_id'] as int,
    );
  }
}
