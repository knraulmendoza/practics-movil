import 'package:flutter/cupertino.dart';
import 'package:practics_movil/src/domain/base/BaseEntity.dart';

class RolEntity extends Entity {
  String _name, _description;
  String get name => this._name;
  String get description => this._description;
  set name(String name) => this._name = name;
  set description(String description) => this._description = description;
  RolEntity({int id, @required String name, @required String description})
      : super(id) {
    this._name = name;
    this._description = description;
  }
  factory RolEntity.fromJson(Map<String, dynamic> json) {
    return RolEntity(
      id: json['id'] as int,
      name: json['visual_name'] as String,
      description: json['description'] as String,
    );
  }

  // factory UserEntity.fromJsonSql(Map<String, dynamic> json) {
  //   return UserEntity(
  //       id: json['id'] as int,
  //       userName: json['user_name'] as String,
  //       password: json['password'] as String);
  // }

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'name': this.name,
      'description': this.description,
    };
    map.addAll(super.toJson());

    return map;
  }

  // @override
  // Map<String, dynamic> toJsonSQlite() {
  //   Map<String, dynamic> map = {
  //     'user_name': this.userName,
  //     'password': this.password,
  //   };
  //   map.addAll(super.toJsonSQlite());
  //   return map;
  // }
}
