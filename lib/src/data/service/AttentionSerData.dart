import 'package:hive/hive.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/data/dominio/Attention.dart';
import 'package:practics_movil/src/data/hive/HiveBoxes.dart';
import 'package:practics_movil/src/data/service/ResponseSerData.dart';

class AttentionSerData {
  final attentions = Hive.box<Attention>(HiveBoxes.attentions);
  Future<ResponseSerData> add(Attention attention) async {
    final resp =
        ResponseSerData(message: 'Se registro la atención', state: true);
    try {
      resp.id = await this.attentions.add(attention);
      // if (this.validateAttention(attention.patient.document)==null) await this.attentions.add(attention);
      // else {
      //   resp.message = 'Este paciente ya existe';
      //   resp.state = false;
      // }
    } catch (e) {
      resp.message = 'Hubo un error en registrar la atención';
      resp.state = false;
    }
    return resp;
  }

  bool delete(int index) {
    bool ok = true;
    try {
      this.attentions.deleteAt(index);
    } catch (e) {
      ok = false;
    }
    return ok;
  }

  searchPatient(String document) {
    return this.getAll().firstWhere(
          (attention) => attention.patient.document.indexOf(document) > -1,
          orElse: () => null,
        );
  }

  int getAttentionIndex(String document) {
    return this
        .getAll()
        .indexWhere((attention) => attention.patient.document == document);
  }

  Future<void> deleting() async {
    await this.attentions.clear();
  }

  validateAttention(String document) {
    return this.getAll().firstWhere(
          (attention) => attention.patient.document.compareTo(document) == 0,
          orElse: () => null,
        );
  }

  List<Attention> getAll() {
    List<Attention> list = [];
    for (var i = 0; i < this.attentions.length; i++) {
      list.add(this.attentions.getAt(i));
    }
    return list;
  }

  List<Attention> getAllDateNow() {
    return this
        .getAll()
        .where((attention) =>
            attention.date.compareTo(Jiffy().format('yyy-MM-dd')) == 0)
        .toList();
  }

  Attention getAttention(int index) {
    return this.attentions.getAt(index);
  }

  Attention last() {
    return this.attentions.getAt(this.attentions.length - 1);
  }

  List<Attention> getAllNoSent() {
    return this
        .getAll()
        .where((attention) => attention.stateSent == false)
        .toList();
  }

  List<Attention> getAllSent() {
    return this
        .getAll()
        .where((attention) => attention.stateSent == true)
        .toList();
  }

  void updateAttention(int index, String state) {
    this.getAttention(index).state = state;
    this.getAttention(index).stateSent = true;
    this.getAttention(index).save();
  }
}

final attentionSerData = AttentionSerData();
