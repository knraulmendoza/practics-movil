import 'package:flutter/cupertino.dart';

class ResponseSerData {
  String message;
  bool state;
  int id;
  ResponseSerData({@required this.message, @required this.state, this.id});
}
