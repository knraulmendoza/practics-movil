import 'package:hive/hive.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/data/dominio/Assistence.dart';
import 'package:practics_movil/src/data/hive/HiveBoxes.dart';
import 'package:practics_movil/src/data/service/ResponseSerData.dart';

class AssistenceSerData {
  final assistences = Hive.box<Assistence>(HiveBoxes.assistences);
  Future<bool> add(Assistence assistence) async {
    bool ok = true;
    try {
      await this.assistences.add(assistence);
    } catch (e) {
      ok = false;
    }
    return ok;
  }

  void sent() {
    this.getAll().forEach((assistence) {
      assistence.stateSent = true;
      assistence.save();
    });
  }

  bool validateAssistences() {
    bool state = true;
    for (var i = 0; i < this.assistences.length; i++) {
      if (this
              .assistences
              .getAt(i)
              .date
              .compareTo(Jiffy().format('yyy-MM-dd')) ==
          0) state = false;
    }
    return state;
  }

  Future<ResponseSerData> addAll(List<Assistence> assistences) async {
    final responseSerData = new ResponseSerData(
        message: 'Assistencia registradas de manera correcta', state: true);
    try {
      responseSerData.state = validateAssistences();
      if (responseSerData.state) await this.assistences.addAll(assistences);
    } catch (e) {
      responseSerData.message = 'Hubo un error, intentelo de nuevo';
      responseSerData.state = false;
    }
    return responseSerData;
  }

  List<Assistence> getAll() {
    List<Assistence> list = [];
    for (var i = 0; i < this.assistences.length; i++) {
      list.add(this.assistences.getAt(i));
    }
    return list;
  }

  List<Assistence> getAllNoSent() {
    return this
        .getAll()
        .where((assistence) => assistence.stateSent == false)
        .toList();
  }

  sentData() {
    for (var i = 0; i < this.getAll().length; i++) {
      this.assistences.getAt(i).stateSent = true;
      this.assistences.getAt(i).save();
    }
  }

  Future<void> deleting() async {
    await this.assistences.clear();
  }

  bool delete(int index) {
    bool ok = true;
    try {
      this.assistences.deleteAt(index);
    } catch (e) {
      ok = false;
    }
    return ok;
  }
}

final assistenceSerData = new AssistenceSerData();
