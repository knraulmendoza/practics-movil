import 'package:hive/hive.dart';
import 'package:practics_movil/src/data/dominio/Assistence.dart';
import 'package:practics_movil/src/data/dominio/Attention.dart';
import 'package:practics_movil/src/data/dominio/Module.dart';
import 'package:practics_movil/src/data/dominio/Patient.dart';
import 'package:practics_movil/src/data/dominio/Procedure.dart';
import 'package:practics_movil/src/data/hive/HiveBoxes.dart';

List<void> openAdapter() {
  return [
    Hive.registerAdapter(AssistenceAdapter()),
    Hive.registerAdapter(ModuleAdapter()),
    Hive.registerAdapter(ProcedureAdapter()),
    Hive.registerAdapter(PatientAdapter()),
    Hive.registerAdapter(AttentionAdapter()),
  ];
}

Future<List<Box>> openBox() async {
  return [
    await Hive.openBox<Assistence>(HiveBoxes.assistences),
    await Hive.openBox<Module>(HiveBoxes.modules),
    await Hive.openBox<Procedure>(HiveBoxes.procedures),
    await Hive.openBox<Patient>(HiveBoxes.patients),
    await Hive.openBox<Attention>(HiveBoxes.attentions)
  ];
}
