class HiveBoxes {
  static String assistences = 'assistences';
  static String attentions = 'attentions';
  static String patients = 'patiens';
  static String procedures = 'procedures';
  static String modules = 'modules';
  static List<String> boxes = [
    assistences,
    attentions,
    patients,
    procedures,
    modules
  ];
}
