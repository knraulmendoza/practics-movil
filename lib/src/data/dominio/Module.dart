import 'package:hive/hive.dart';

part 'Module.g.dart';

@HiveType(typeId: 1)
class Module extends HiveObject {
  @HiveField(0)
  String id;
  @HiveField(1)
  String name;
  @HiveField(2)
  String code;
  @HiveField(3)
  String description;
  @HiveField(4)
  int subjectId;
}
