// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Patient.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PatientAdapter extends TypeAdapter<Patient> {
  @override
  final int typeId = 3;

  @override
  Patient read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Patient()
      ..document = fields[0] as String
      ..name = fields[1] as String
      ..origin = fields[2] as String
      ..phone = fields[3] as String
      ..documentType = fields[4] as int
      ..gender = fields[5] as int
      ..age = fields[6] as int
      ..status = fields[7] as int
      ..academicLevel = fields[8] as int
      ..emplotmentStituation = fields[9] as int
      ..stateSent = fields[10] as bool;
  }

  @override
  void write(BinaryWriter writer, Patient obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.document)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.origin)
      ..writeByte(3)
      ..write(obj.phone)
      ..writeByte(4)
      ..write(obj.documentType)
      ..writeByte(5)
      ..write(obj.gender)
      ..writeByte(6)
      ..write(obj.age)
      ..writeByte(7)
      ..write(obj.status)
      ..writeByte(8)
      ..write(obj.academicLevel)
      ..writeByte(9)
      ..write(obj.emplotmentStituation)
      ..writeByte(10)
      ..write(obj.stateSent);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PatientAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
