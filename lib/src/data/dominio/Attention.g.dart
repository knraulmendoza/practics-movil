// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Attention.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AttentionAdapter extends TypeAdapter<Attention> {
  @override
  final int typeId = 4;

  @override
  Attention read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Attention()
      ..date = fields[0] as String
      ..state = fields[1] as String
      ..patientId = fields[2] as int
      ..patient = fields[3] as Patient
      ..procedures = (fields[4] as List)?.cast<Procedure>()
      ..proceduresId = (fields[5] as List)?.cast<int>()
      ..stateSent = fields[6] as bool;
  }

  @override
  void write(BinaryWriter writer, Attention obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.date)
      ..writeByte(1)
      ..write(obj.state)
      ..writeByte(2)
      ..write(obj.patientId)
      ..writeByte(3)
      ..write(obj.patient)
      ..writeByte(4)
      ..write(obj.procedures)
      ..writeByte(5)
      ..write(obj.proceduresId)
      ..writeByte(6)
      ..write(obj.stateSent);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AttentionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
