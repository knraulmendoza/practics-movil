// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Assistence.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AssistenceAdapter extends TypeAdapter<Assistence> {
  @override
  final int typeId = 0;

  @override
  Assistence read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Assistence()
      ..assist = fields[0] as int
      ..studentId = fields[1] as int
      ..observation = fields[2] as String
      ..date = fields[3] as String
      ..stateSent = fields[4] as bool;
  }

  @override
  void write(BinaryWriter writer, Assistence obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.assist)
      ..writeByte(1)
      ..write(obj.studentId)
      ..writeByte(2)
      ..write(obj.observation)
      ..writeByte(3)
      ..write(obj.date)
      ..writeByte(4)
      ..write(obj.stateSent);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AssistenceAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
