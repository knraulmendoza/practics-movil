import 'package:hive/hive.dart';

part 'Assistence.g.dart';

@HiveType(typeId: 0)
class Assistence extends HiveObject {
  @HiveField(0)
  int assist;
  @HiveField(1)
  int studentId;
  @HiveField(2)
  String observation;
  @HiveField(3)
  String date;
  @HiveField(4)
  bool stateSent = false;
}
