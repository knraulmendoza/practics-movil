import 'package:hive/hive.dart';

part 'Patient.g.dart';

@HiveType(typeId: 3)
class Patient extends HiveObject {
  @HiveField(0)
  String document;
  @HiveField(1)
  String name;
  @HiveField(2)
  String origin;
  @HiveField(3)
  String phone;
  @HiveField(4)
  int documentType;
  @HiveField(5)
  int gender;
  @HiveField(6)
  int age;
  @HiveField(7)
  int status;
  @HiveField(8)
  int academicLevel;
  @HiveField(9)
  int emplotmentStituation;
  @HiveField(10)
  bool stateSent = false;
}
