import 'package:hive/hive.dart';

import 'Patient.dart';
import 'Procedure.dart';

part 'Attention.g.dart';

@HiveType(typeId: 4)
class Attention extends HiveObject {
  @HiveField(0)
  String date;
  @HiveField(1)
  String state;
  @HiveField(2)
  int patientId;
  @HiveField(3)
  Patient patient;
  @HiveField(4)
  List<Procedure> procedures;
  @HiveField(5)
  List<int> proceduresId;
  @HiveField(6)
  bool stateSent = false;
}
