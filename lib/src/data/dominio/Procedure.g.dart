// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Procedure.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ProcedureAdapter extends TypeAdapter<Procedure> {
  @override
  final int typeId = 2;

  @override
  Procedure read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Procedure()
      ..id = fields[0] as int
      ..name = fields[1] as String
      ..code = fields[2] as String
      ..description = fields[3] as String
      ..module = fields[4] as Module;
  }

  @override
  void write(BinaryWriter writer, Procedure obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.code)
      ..writeByte(3)
      ..write(obj.description)
      ..writeByte(4)
      ..write(obj.module);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProcedureAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
