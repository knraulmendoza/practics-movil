import 'package:hive/hive.dart';

import 'Module.dart';

part 'Procedure.g.dart';

@HiveType(typeId: 2)
class Procedure extends HiveObject {
  @HiveField(0)
  int id;
  @HiveField(1)
  String name;
  @HiveField(2)
  String code;
  @HiveField(3)
  String description;
  @HiveField(4)
  Module module;
}
