import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:jiffy/jiffy.dart';
import 'package:practics_movil/src/helpers/Connection.dart';
import 'package:practics_movil/src/presentation/controllers/MenuGlobalController.dart';
import 'package:practics_movil/src/presentation/controllers/GlobalsController.dart';
import 'package:practics_movil/src/services/LoginService.dart';
import './src/presentation/routes/route.dart';
import 'src/data/hive/initHive.dart';

void main() async {
  await GetStorage.init();
  await Hive.initFlutter();
  openAdapter();
  await openBox();
  String date = GetStorage().read<String>('date');
  if (date == null) {
    GetStorage().write('date', Jiffy().format('yyy-MM-dd'));
  } else {
    if (date.compareTo(Jiffy().format('yyy-MM-dd')) != 0) {
      loginService.deleteGetLocal();
    }
  }
  if (GetStorage().read('token') != null) {
    if (await Connection.connection)
      loginService.validateToken();
    else
      GetStorage().write('authenticated', false);
  }
  if (GetStorage().read('connection') == null) {
    GetStorage().write('connection', true);
  }
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Get.put(GlobalsController());
    Get.put(MenuGlobalController());
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Practics',
      theme: ThemeData(
                primaryColor: Colors.white,
        primaryIconTheme: IconThemeData(
          color: Colors.blue[600],
        ),
        primaryTextTheme: TextTheme(
          headline6: TextStyle(
            color: Colors.blue[600],
          ),
          bodyText1: TextStyle(
            color: Colors.white,
          ),
          bodyText2: TextStyle(
            color: Colors.blue,
          ),
        ),
      ),
      // accentColor: Colors.blueAccent),
      getPages: getAppRoutes(),
      // home: auth ? menuGlobal.currentPage.value : LoginPage(),
      initialRoute: '/',
      locale: Locale('es', 'ES'),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('es', 'ES'), // Thai
      ],
    );
  }
}
